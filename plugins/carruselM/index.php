<?php
/* Carrusel Morgan v1.0 */

$img_dir = 'images/';

$imgs = array(
	array('file' => 'carrucel_1-carrucel__1_.jpg', "title" => '', "index" => 1 ),
	array('file' => 'Informational_Interviews_Yezenia_Russ_Finance-Informational_Interviews_Yezenia___Russ_Finance.jpg', "title" => '', "index" => 2 ),
	array('file' => 'J_J_1-J_J_1.jpg', "title" => '', "index" => 3 ),
	array('file' => 'John_Bryan_Alberto_Gibram-John_Bryan_Alberto_Gibram.jpg', "title" => '', "index" => 6 ),
	array('file' => 'Linchy_Grace_Fernanda-Linchy__Grace__Fernanda.jpg', "title" => '', "index" => 7 ),
	array('file' => 'MG_0036_1-_MG_0036__1_.jpg', "title" => '', "index" => 5 ),
	array('file' => 'Refreshmints-Refreshmints.jpg', "title" => '', "index" => 4 ),

	array('file' => 'carrucel_1-carrucel__1_.jpg', "title" => '', "index" => 1 ),
	array('file' => 'Informational_Interviews_Yezenia_Russ_Finance-Informational_Interviews_Yezenia___Russ_Finance.jpg', "title" => '', "index" => 2 ),
	array('file' => 'J_J_1-J_J_1.jpg', "title" => '', "index" => 3 ),
	array('file' => 'John_Bryan_Alberto_Gibram-John_Bryan_Alberto_Gibram.jpg', "title" => '', "index" => 6 ),
	array('file' => 'Linchy_Grace_Fernanda-Linchy__Grace__Fernanda.jpg', "title" => '', "index" => 7 ),
	array('file' => 'MG_0036_1-_MG_0036__1_.jpg', "title" => '', "index" => 5 ),
	array('file' => 'Refreshmints-Refreshmints.jpg', "title" => '', "index" => 4 ),
	array('file' => 'carrucel_1-carrucel__1_.jpg', "title" => '', "index" => 1 ),
	array('file' => 'Informational_Interviews_Yezenia_Russ_Finance-Informational_Interviews_Yezenia___Russ_Finance.jpg', "title" => '', "index" => 2 ),
	array('file' => 'J_J_1-J_J_1.jpg', "title" => '', "index" => 3 ),
	array('file' => 'John_Bryan_Alberto_Gibram-John_Bryan_Alberto_Gibram.jpg', "title" => '', "index" => 6 ),
	array('file' => 'Linchy_Grace_Fernanda-Linchy__Grace__Fernanda.jpg', "title" => '', "index" => 7 ),
	array('file' => 'MG_0036_1-_MG_0036__1_.jpg', "title" => '', "index" => 5 ),
	array('file' => 'Refreshmints-Refreshmints.jpg', "title" => '', "index" => 4 ),
);

?>

<div class="crrslM" style="width: 60%;">
	<div class="crrslM_cont">
		<div class="crrslM_elem"><img src="" id="crrslM_img"></div>
		<div class="crrslM_button cm_preview"><div class="ico"></div></div>
		<div class="crrslM_button cm_next"><div class="ico"></div></div>
	</div>

	<!-- list thumbs -->
	<div class="crrslM_thumbs">
		<div class="crrslM_thumbs_left"><div class="ico"></div></div><?php
		?><div class="crrslM_thumbs_cont">
			<ol id="crrslMt_lits" class=""><?php

				$n=1;
				foreach ($imgs as $et => $r) {

					echo "\n";

					$active = '';
					if($n==1){ $active = ' class="active"'; }

					?><li<?php echo $active; ?>><?php
						?><div class="crrslM_thumb" data="<?php echo $img_dir.$r['file']; ?>" id="img-<?php echo $n; ?>"><?php
							?><img src="<?php echo 'images/'.$r['file']; ?>" alt="" data-index="<?php echo $n; ?>"><?php
							?><span><?php echo $n; ?></span><?php
						?></div><?php
					?></li><?php
					$n++;
				}

			?></ol>
		</div>
		<div class="crrslM_thumbs_rigth"><div class="ico"></div></div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="css/morgan_carrusel.css?v=159266">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="js/morgan_carrusel.js"></script>

<script type="text/javascript">

var carruselM = null;

window.onload = function() {
	carruselM = carruselM_init();
}

</script>


