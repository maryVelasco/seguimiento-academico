/* Carrusel Morgan v1.0 */

// inicia el carrusel

var carruselM_block = 1;
var carruselM_thumbs = 1;

function carruselM_init(e){
	console.log(e);

	$('.crrslM_thumb').each( function(indx,e){
		var id = '#'+e.attributes['id'].value;
		$(id).attr( 'data_indx', indx+1 );
	} );

	carruselM_set(1);

	if( carruselM_max()<=5 ){
		carruselM_thumbs_arrow_disable();
	}

	return true;
}
// obtiene el numero de imagenes en el carrusel
function carruselM_max(){
	var l = $('.crrslM_thumb').length;
	if(l==undefined){ return 0; }
	return l;
}
// activa una imagen especifica
function carruselM_set(indx){

	var e = $('.crrslM_thumb')[ indx-1 ];
	if( e==undefined ){ return false; }

	// imagen
	$('#crrslM_img' ).attr( 'src', e.attributes['data'].value );
	$('.crrslM_elem').attr( 'data_indx', e.attributes['data_indx'].value );

	carruselM_block = Math.ceil( indx/5 );
	carruselM_thumbs = carruselM_block;

	carruselM_set_li_off();
	carruselM_set_li_on( indx );

	return true;
}
// coloca en off todos los thumbs
function carruselM_set_li_off(){
	$('.crrslM_thumbs ol li').removeClass('active');
	$('.crrslM_thumbs ol li').css( 'display','none' );

	return true;
}
// activa un thumb especifico
function carruselM_set_li_on(indx){
	//$('.crrslM_thumbs ol li')[ (indx-1) ].addClass('active');

	$('.crrslM_thumbs ol li').each( function(i,e){
		if( (indx-1) == i ){
			e.className = "active";
		}
	} );

	var bfin = carruselM_block * 5;
	var bini = bfin - 5;

	$('.crrslM_thumbs ol li').each( function(i,e){
		if( i>=bini && i<bfin ){
			e.setAttribute( 'style', '' );
		}
	} );

	return true;
}
// muestra imagen siguiente
function carruselM_set_next(){
	// obteniendo indx actual
	var n = $('.crrslM_elem').attr('data_indx');
	// obteniendo el numero maximo de items
	var max = carruselM_max();

	// validando si es el ultimo
	if(n>=max){ n = 0; }
	if(n<0){ n = 0; }
	n++;
	carruselM_set(n);
	return true;
}
// muestra imagen previa
function carruselM_set_preview(){
	// obteniendo indx actual
	var n = $('.crrslM_elem').attr('data_indx');
	// obteniendo el numero maximo de items
	var max = carruselM_max();

	// validando si es el ultimo
	n--;
	if(n<=0){ n = max; }
	carruselM_set(n);
	return true;
}
// muestra el siguiente bloque thumbs
function carruselM_thumbs_next(){
	carruselM_thumbs++;
	carruselM_set_li_off();

	var bfin = carruselM_thumbs * 5;
	var bini = bfin - 5;

	if( bini>carruselM_max() ){
		bfin = 5;
		bini = 0;
		carruselM_thumbs = 1;
	}

	$('.crrslM_thumbs ol li').each( function(i,e){
		if( i>=bini && i<bfin ){
			e.setAttribute( 'style', '' );
		}
	} );

	return true;
}
// muestra el siguiente bloque thumbs
function carruselM_thumbs_preview(){
	carruselM_thumbs--;
	if( carruselM_thumbs<1 ){ carruselM_thumbs = Math.ceil( carruselM_max()/5 ); }
	carruselM_set_li_off();

	var bfin = carruselM_thumbs * 5;
	var bini = bfin - 5;

	$('.crrslM_thumbs ol li').each( function(i,e){
		if( i>=bini && i<bfin ){
			e.setAttribute( 'style', '' );
		}
	} );

	return true;
}
// desabilita las flechas de thumbs
function carruselM_thumbs_arrow_disable(){
	document.getElementsByClassName('crrslM_thumbs_left')[0].setAttribute('style','display:none;')
	document.getElementsByClassName('crrslM_thumbs_rigth')[0].setAttribute('style','display:none;')
}

$('.crrslM_thumb').click( function(){ carruselM_set( this.attributes['data_indx'].value ); } );
$('.cm_next').click( function(){      carruselM_set_next(); } );
$('.cm_preview').click( function(){	  carruselM_set_preview(); } );

$('.crrslM_thumbs_rigth').click( function(){ carruselM_thumbs_next(); } );
$('.crrslM_thumbs_left').click( function(){ carruselM_thumbs_preview(); } );
