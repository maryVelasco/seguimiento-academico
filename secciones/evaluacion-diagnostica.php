<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <img src="../images/inicio-img/tira-directores.jpg" class="img-fluid" />
        <h1 class="my-4">Evaluación diagnóstica</h1>
      </div>
      <hr />
      <br /><br />

      <div class="container"><!-- info -->
        <div class="container">
          <div class="row">
              <div class="col-md-4 text-center">
                <div class="circulo mb-2" style="background-color: #e6e6e6; margin-bottom: 10px;">
                  <img src="docs/evaluacion/alumno.png" height="100" />
                </div>
                  <button type="button" class="btn btn-info mt-4">Materiales para el alumno</button>
              </div>
              <div class="col-md-4">
               <!-- <div class="circulo" style="background-color: #e6e6e6; margin-bottom: 10px;">
                  <img src="docs/evaluacion" height="100" />
              </div> -->
            </div>
            <div class="col-md-4 text-center">
                <div class="circulo" style="background-color: #e6e6e6; margin-bottom: 10px;">
                  <img src="docs/evaluacion/edificio.png" height="100" />
                </div>
                <button type="button" class="btn btn-info mt-4">Materiales para planteles</button>
        </div> 
          </div>
        </div>
    </div>
      <!-- info -->
    </div>
    <!-- fin Content -->

    <div style="height: 250px;">&nbsp;</div>

    <!-- Footer -->
    <?php include('footer.php');?>
