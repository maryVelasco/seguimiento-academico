<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4 container_gral">
      <div class="container text-center">
       <!--  <img src="docs/img-secciones/IMG_5450.JPG" class="img-fluid" /> -->
        <h1 class="my-4">Estrategias de seguimiento académico por plantel</h1>
        <hr />
        <br /><br />
      </div>

      <div class="container bg-light p-4 rounded">
        <!-- planteles <-->

        <div class="row">
          <div class="size_dinamic plantel-caja plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-1.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-1-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-1-SEA.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-1-SEA-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-2.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-2-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-3.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-3-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-4.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-4-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-5.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-5-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-6.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-6-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-7.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-7-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-8.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-8-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-09.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-9-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-10.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-10-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-11.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-11-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-12.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-12-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-13.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-13-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-14.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-14-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-15.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-15-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-16.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-16-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-17.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-17-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-18.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-18-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-19.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-19-colbach.svg"
                class="logp"
            /></a>
          </div>
          <div class="size_dinamic plantel-caja">
            <a href="docs/Estrategias-Directivos/Plantel-20.pdf">
              <img
                src="docs/img-secciones/logos-planteles/plantel-20-colbach.svg"
                class="logp"
            /></a>
          </div>
        </div>
      </div>
      <!-- fin Content -->
    </div>

    <!-- Footer -->
<?php include('footer.php');?>