<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" />
        <h1 class="my-4">Docentes</h1>
      </div>
      <hr />

      <div class="container">
        <!-- info -->
        <div class="cajaSecciones">
          <ul>
            <li class="mb-2">
              <a href="cont-fundamentales-escolarizado.html"
                >Contenidos esenciales (escolarizado)</a
              >
            </li>
            <li class="mb-2">
              <a href="foro-salud-mental-adolescente.html"
                >2º Foro Académico para Orientadores y Tutores de Acompañamiento</a
              >
            </li>
            <li>
              <a href="orientaciones-segimiento-academico.html"
                >Orientaciones para el seguimiento académico y recursos
                didácticos
              </a>
            </li>
            <li>
              <a href="orientaciones-evaluacion-aprendizaje.html"
                >Orientaciones para la evaluación de los aprendizajes</a
              >
            </li>
            <li>
              <a href="pro-estudios-metodologicas.html"
                >Orientaciones metodológicas</a
              >
            </li>

            <li class="mb-2">
              <a href="apoyo-psicologico-docentes.html">
                Materiales de apoyo psicológico y emocional</a
              >
            </li>
            <li class="mb-2">
              <a href="maestras-maestros-casa.html">
                Maestras y maestros en casa
              </a>
            </li>
            <li class="mb-2">
              <a href="formacion-actualizacion-docente.html"
                >Formación y actualización docente
              </a>
            </li>
            <li class="mb-2">
              <a href="materiales-gestion-escolar.html"
                >Materiales para apoyar la gestión escolar</a
              >
            </li>
            <li class="mb-2">
              <a href="foro-salud-mental-adolescente.html"
                >2º Foro Académico Orientación y Tutoría</a
              >
            </li>
          </ul>
        </div>
      </div>
      <!-- info -->
    </div>
    <!-- fin Content -->

    <!-- Footer -->
    <footer>
      <div class="footergob">
        <div class="list-info">
          <div class="container">
            <div class="row">
              <div class="col-sm-3">
                <img
                  data-v-9e928f9a=""
                  src="https://framework-gb.cdn.gob.mx/landing/img/logoheader.svg"
                  href="/"
                  alt="logo gobierno de méxico"
                  class="logo_footer"
                  style="max-width: 90%"
                />
              </div>

              <div class="col-sm-3">
                <ul>
                  <li>
                    <h5>Enlaces</h5>
                  </li>
                  <li>
                    <a
                      href="https://participa.gob.mx"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Participa</a
                    >
                  </li>
                  <li>
                    <a
                      href="https://www.gob.mx/publicaciones"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Publicaciones Oficiales</a
                    >
                  </li>
                  <li>
                    <a
                      href="http://www.ordenjuridico.gob.mx"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Marco Jurídico</a
                    >
                  </li>
                  <li>
                    <a
                      href="https://consultapublicamx.inai.org.mx/vut-web/"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Plataforma Nacional de Transparencia</a
                    >
                  </li>
                </ul>
              </div>
              <div class="col-sm-3">
                <h5>¿Qué es gob.mx?</h5>

                <p>
                  Es el portal único de trámites, información y participación
                  ciudadana.
                  <a href="https://www.gob.mx/que-es-gobmx">Leer más</a>
                </p>
              </div>
              <div class="col-sm-3">
                <h5>Contacto</h5>
                <p>Dudas e información a</p>
                <p>mesdeayuda@bachilleres.edu.mx</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        style="
          background-image: url('../gobmx/pleca.png');
          background-color: #13322b;
          background-repeat: repeat-x;
          height: 33px;
        "
      ></div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../scripts-boots/jquery/jquery.min.js"></script>
    <script src="../scripts-boots/bootstrap/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
