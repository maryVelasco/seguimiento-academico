<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!-- <img src="docs/img-secciones/tira-maestros.jpg" class="img-fluid" /> -->
        <h1 class="my-4" style="width: 90%;">
          Habilidades socioemocionales
        </h1>
        <hr />
        <br />
      </div>
      <!--tabla contenidos--->
      <div class="container">
        <div style="margin-bottom: 50px; text-align: center;">
          <a
            href="docs/apoyo-psic-docentes/Presentacion.pdf"
            target="pdfreader"
          >
            <img src="docs/apoyo-psic-docentes/presentacion.jpg"
          /></a>
        </div>
        <!--fin tabla 1 -->

        <!--submenus -->
        <div class="container">
          <div class="row">
            <div class="col-md-4 text-center">
              <div>
                <img src="docs/apoyo-psic-docentes/image004.png" height="150" />
              </div>
              <div
                class="btn-group"
                role="group"
                aria-label="Button group with nested dropdown"
              >
                <div class="btn-group" role="group">
                  <button
                    id="btnGroupDrop1"
                    type="button"
                    class="btn dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    style="background: #ff5757;"
                  >
                    Desarrollo personal
                  </button>
                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Tiempo-leer.pdf"
                      >Tiempo para leer</a
                    >
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 text-center">
              <div>
                <img src="docs/apoyo-psic-docentes/image005.png" height="150" />
              </div>
              <div
                class="btn-group"
                role="group"
                aria-label="Button group with nested dropdown"
              >
                <div class="btn-group" role="group">
                  <button
                    id="btnGroupDrop1"
                    type="button"
                    class="btn dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    style="background: #58aea2;"
                  >
                    Equilibrio emocional
                  </button>
                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Botones.pdf"
                      target="pdfreader"
                      >Botones emocionales</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/cuantas-emociones.pdf"
                      target="pdfreader"
                      >Emociones y docencia</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Emociones-decisiones.pdf"
                      target="pdfreader"
                      >Emociones y decisiones</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/empatia.pdf"
                      target="pdfreader"
                      >La empatía</a
                    >

                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Manejo-ansiedad.pdf"
                      target="pdfreader"
                      >Manejo de ansiedad</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/manejo-estres.pdf"
                      target="pdfreader"
                      >Manejo de estrés</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Momento-relajacion.pdf"
                      target="pdfreader"
                      >Momento de relajación</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/RecursosApoyo.pdf"
                      target="pdfreader"
                      >Recursos de Apoyo <br />emocional y psicológico</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/tecnicas-respiracion.pdf"
                      target="pdfreader"
                      >Técnicas de respiración</a
                    >
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 text-center">
              <div>
                <img src="docs/apoyo-psic-docentes/balance.png" height="150" />
              </div>
              <div
                class="btn-group"
                role="group"
                aria-label="Button group with nested dropdown"
              >
                <div class="btn-group" role="group">
                  <button
                    id="btnGroupDrop1"
                    type="button"
                    class="btn dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    style="background: #efcf10;"
                  >
                    Balance <br />vida - trabajo
                  </button>
                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Administracon-Tiempo.pdf"
                      target="pdfreader"
                      >Administración del tiempo</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Conciliar-trabajo-familia.pdf"
                      target="pdfreader"
                      >Conciliar trabajo y familia</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Cuidado-dinero.pdf"
                      target="pdfreader"
                      >Cuidado del dinero</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Solucion-conflictos.pdf"
                      target="pdfreader"
                      >Solución asertiva <br />de conflictos</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/5-tips-trabajo-casa.pdf"
                      target="pdfreader"
                      >5 Tip ́s para <br>trabajo en casa</a
                    >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style="height: 300px;">&nbsp;</div>
        <!-- ------------------------------------------------------------------------- -->
      </div>
      <!-- fin container tabla -->
      <!-- fin Content -->
    </div>

    <!-- Footer -->
  <?php include('footer.php');?>
