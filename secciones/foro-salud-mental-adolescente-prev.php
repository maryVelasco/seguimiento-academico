<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!--  <img src="docs/foro/foro-academico.jpg" class="img-fluid" /> -->
        <h1 class="my-4" style="width: 90%">
          Foros Académicos para Orientadores y Tutores de Acompañamiento 
        </h1>
        <h4>“SALUD MENTAL Y EMOCIONAL DE LOS ADOLESCENTES”</h4>
        <hr />
        <br />
      </div>
      <!--tabla contenidos--->
      <div class="container">
        <div style="margin-bottom: 50px; text-align: center">
          <a href="docs/foro/SemblanzaForo2019.pdf" target="pdfreader">
            <img src="docs/foro/foro-dialogos.jpg" width="200"
          /></a>
          <h4>Semblanza del 1er Foro</h4>
        </div>
        <!--fin tabla 1 -->

        <!-- recursos PDF -->
        <div class="container">
          <div class="row mb-5">
            <!-- elemento -->
            <div class="col-md-4 text-center">
              <!-- icono -->
              <div class="mb-2">
                <img src="docs/foro/ico-foro1.jpg" height="120" />
              </div>

              <!-- menu -->
              <div class="btn-group" role="group" aria-label="Button group with nested dropdown" >
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop_1" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    style="background: #dbc6eb">Programa del evento</button>
                  <!-- opciones menu -->
                  <div class="dropdown-menu menu_morgan" aria-labelledby="btnGroupDrop_1" >
                    <ul>
                      <li><a class="dropdown-item" href="docs/foro/objetivos.jpg" target="_blank">Objetivos y actividades</a></li>
                      <li><a class="dropdown-item" href="docs/foro/ponentes.pdf" target="_blank">Ponentes</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- elemento -->
            <div class="col-md-4 text-center">
              <!-- icono -->
              <div class="mb-2">
                <img src="docs/foro/ico-foro2.jpg" height="120" />
              </div>

              <!-- menu -->
              <div class="btn-group" role="group" aria-label="Button group with nested dropdown" >
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop_2" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    style="background: #f96d80" >Materiales de consulta</button>
                  <!-- opciones menu -->
                  <div class="dropdown-menu menu_morgan" aria-labelledby="btnGroupDrop_2" >
                    <ul>
                      <li>
                        <label>Lecturas previas al foro</label>
                        <ul>
                          <li>
                            <a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/1-Emociones-sentimientos-profesor.pdf"
                            title="Emociones y sentimientos del profesor">
                            Emociones y sentimientos del profesor</a></li>
                          <li>
                            <a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/3-la-generacion-n-docente.pdf" title="La generación N y el Docente">La generación N y el Docente</a></li>
                          <li>
                            <a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/5-competencias-emocionales-docente-desp-prof.pdf" title="Las competencias emocionales del docente">
                            Las competencias emocionales del docente</a></li>
                          <li>
                            <a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/6-desarrollo-empatia-mejorar-ambiente-escolar.pdf" title="Desarrollo de la empatía para mejorar el ambiente escolar">
                            Desarrollo de la empatía para mejorar el ambiente escolar</a></li>
                          <li>
                            <a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/7-emociones-sentimientos-profesor.pdf" title="Emociones y sentimientos del profesor cap. 3">
                            Emociones y sentimientos del profesor cap. 3</a></li>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/9-orientacio-psico-educ-emocional.pdf" title="Orientación psicopedagógica y educación emocional">Orientación psicopedagógica y educación emocional</a></li>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/12-rec-tutoria-acomp.pdf" title="Recomendaciones para la tutoria de acompañamiento">Recomendaciones para la tutoria de acompañamiento</a></li>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/14-oms-violencia-vs-mujeres.pdf" title="Comprender y abordar la violencia contra las mujeres">Comprender y abordar la violencia contra las mujeres</a></li>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/15-protocolo-as-hs.pdf" title="Protocolo as hs">Protocolo as hs</a></li>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/16-embarazo-adolescente.pdf" title="Embarazo adolescente">Embarazo adolescente</a></li>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/lect-previas-foro/19-protocolo-atencion-embarazada-menor-15.pdf" title="Protocolo de atención a la embarazada menor de 15 años">Protocolo de atención a la embarazada menor de 15 años</a></li>
                        </ul>
                      </li>
                      <li>
                        <label>Material recomendado<br /> por ponentes</label>
                        <ul>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/4-notasParaRepensarLaPracticaDocenteDesdeLaOtredad-.pdf"  title="" >Notas para repensar la práctica docente desde la otredad</a></li>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/8-dimensiones-afectivas-docencia.pdf"                     title="" >Las dimensiones afectivas de la docencia</a></li>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/10-competencias-emocionales.pdf"                          title="" >Las competencias emocionales</a></li>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/11-apoyo-emocional-ansiedad.pdf"                          title="" >Identificando la Ansiedad en nuestro cuerpo y los recursos para afrontarla</a></li>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/13-inf-reconoce-denuncia-as-hs.pdf"                       title="">Reconoce, protégete y denuncia el hs y as</a></li>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/17-manual-salud-mental-educadores.pdf"                    title="">Manual de salud mental para profesionales del ambito educativo</a></li>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/18-programa-accion-salud-mental.pdf"                      title="">Programa de acción en salud mental</a></li>
                            <li><a class="list-group-item" target="_blank" href="docs/foro/recomendado-ponentes/20-salud-mental-adolescentes-media-superior.pdf"          title="">Estado de salud mental de adolescentes que cursan la educación media superior</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- elemento -->
            <div class="col-md-4 text-center">
              <div class="mb-2">
                <img src="docs/foro/ico-foro3.jpg" height="120" />
              </div>
              <div class="btn-group" role="group" aria-label="Button group with nested dropdown" >
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop_3" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    style="background: #a3f7bf" >Conferencias y Conversatorio</button>
                  <div class="dropdown-menu menu_morgan" aria-labelledby="btnGroupDrop_3" >
                    <!--
                    <ul>
                      <li><a class="list-group-item" href="docs/" target="_blank">Presentaciones de ponentes</a></li>
                    </ul>
                    -->
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row mb-5">
            <!-- elemento -->
            <div class="col-md-4 text-center">
              <div class="mb-2">
                <img src="docs/foro/ico-foro4.jpg" height="120" />
              </div>
              <div class="btn-group" role="group" aria-label="Button group with nested dropdown" >
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop_4" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    style="background: #93b5e1" >Retroalimentación del evento</button>
                  <div class="dropdown-menu menu_morgan" aria-labelledby="btnGroupDrop_4" >
                    <ul>
                      <li><a class="list-group-item" target="_blank" href="https://bit.ly/35PkKEa" title="" >Encuesta de satisfacción</a></li>
                      <li>
                        <label>Constancia de participación</label>
                        <ul>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/retroalimenta/Criterios-constancia.pdf" title="" >Criterios para la emisión de constancia</a></li>
                          <li><a class="list-group-item" target="_blank" href="docs/foro/retroalimenta/Formato-estrategia-prevencion.pdf" title="" >Formato para la construcción de estrategia de acompañamiento</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- elemento -->
            <div class="col-md-4 text-center">
            </div>
            <!-- elemento -->
            <div class="col-md-4 text-center">
              <div class="mb-2">
                <img src="docs/foro/ico-foro5.jpg" height="120" />
              </div>
              <div class="btn-group" role="group" aria-label="Button group with nested dropdown" >
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop_6" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    style="background: #f69e7b" >Memoria del evento</button>
                  <div class="dropdown-menu menu_morgan" aria-labelledby="btnGroupDrop_6" >
                    <ul>
                      <li><a class="dropdown-item" href="1er-foro-academico-2019.php" target="pdfreader" >1er Foro Académico 2019</a></li>
                     <li><a class="dropdown-item" href="" target="pdfreader" >2o Foro Académico 2020</a></li>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style="height: 200px">&nbsp;</div>

      </div>
      <!-- fin Content -->
    </div>

    <!-- Footer -->
 <?php include('footer.php');?>

<style type="text/css">
.menu_morgan {}
.menu_morgan ul {list-style-type:none;padding:0;}
.menu_morgan ul li {padding:5px;}
.menu_morgan ul li label {display:block;width:100%;padding:0.25rem 1.5rem;clear:both;font-weight:400;color:#424242;text-align:inherit;white-space:nowrap;background-color:transparent;border:0;margin:0;}
.menu_morgan ul li a {text-decoration:none;font-size:0.9rem;border-radius:0 !important;color:#151515;padding:.25rem 1.5rem;border:0;}
.menu_morgan ul li a:hover {background-color:#F2F2F2;}
.menu_morgan ul li label:hover {background-color:#fff;}
.menu_morgan ul ul {display:none;border:1px solid #dddddd;position:absolute;background-color:#D8D8D8;margin-left:80px;margin-top:-5px;}
.menu_morgan ul ul.derecha {margin-left:100px;margin-top:-30px;}
.menu_morgan ul ul li {width:280px;background-color:transparent;border:0;}
.menu_morgan ul ul li a {padding:0.3rem 1.25rem;background-color:transparent;border:0;}
.menu_morgan ul li:hover ul {display:block;}
</style>
  
