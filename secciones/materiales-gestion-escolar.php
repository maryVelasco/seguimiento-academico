<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="docs/img-secciones/IMG_5450.JPG" class="img-fluid" /> -->
        <h1 class="my-4">Materiales para apoyar la gestión escolar</h1>
      </div>
      <hr />
      <br /><br />

      <div class="container">
        <!-- info -->

        <div class="row">
          <div class="col-md-4 text-center">
            <div>
              <img src="docs/gestion-escolar/plan-educativa.png" height="150" />
            </div>
            <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #bed9fd;"
                >
                  Planeación educativa<br />
                  durante la emergencia
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="docs/gestion-escolar/10-sugerenciasEmergencia.pdf"
                    target="pdfreader"
                    >10 sugerencias para la educación</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/gestion-escolar/Marco-para-guiar.pdf"
                    target="pdfreader"
                    >Un marco para guiar la educación</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/gestion-escolar/Educacion-pandemia.pdf"
                    target="pdfreader"
                    >Educación y pandemia</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/gestion-escolar/Guia-planificar.pdf"
                    target="pdfreader"
                    >Guía para la Planificación</a
                  >
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-4 text-center">
            <!-- <div>
                <img src="docs/apoyo-psic-docentes/balance.png" height="150" />
              </div>
              <div
                class="btn-group"
                role="group"
                aria-label="Button group with nested dropdown"
              >
                <div class="btn-group" role="group">
                  <button
                    id="btnGroupDrop1"
                    type="button"
                    class="btn dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    style="background: #efcf10;"
                  >
                    Balance <br />vida - trabajo
                  </button>
                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Administracon-Tiempo.pdf"
                      target="pdfreader"
                      >Administración del tiempo</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Conciliar-trabajo-familia.pdf"
                      target="pdfreader"
                      >Conciliar trabajo y familia</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Cuidado-dinero.pdf"
                      target="pdfreader"
                      >Cuidado del dinero</a
                    >
                    <a
                      class="dropdown-item"
                      href="docs/apoyo-psic-docentes/Solucion-conflictos.pdf"
                      target="pdfreader"
                      >Solución asertiva <br />de conflictos</a
                    >
                  </div>
                </div>
              </div> -->
          </div>
          
          <div class="col-md-4 text-center">
            <div>
              <img src="docs/gestion-escolar/liderazgo.png" height="150" />
            </div>
            <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #ffc729;"
                >
                  Liderazgo y gestión escolar
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="docs/gestion-escolar/Marco-para-guiar.pdf"
                    target="pdfreader"
                    >Cómo cultivar el liderazgo educativo</a
                  >
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <!-- info -->

      <div style="height: 300px;">&nbsp;</div>
    </div>
    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>
