<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title></title>

    <!-- Bootstrap core CSS -->
    <link
      href="../scripts-boots/bootstrap/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <!-- Custom styles for this template -->
    <link href="../css/estilos.css" rel="stylesheet" />

    <!-- font Awesome CSS -->
    <link
      rel="stylesheet"
      href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700&amp;subset=latin"
      media="all"
    />

    <link rel="shortcut icon" href="images/header/favicon.ico" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script
      async
      src="https://www.googletagmanager.com/gtag/js?id=UA-163121825-2"
    ></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag("js", new Date());
      gtag("config", "UA-163121825-2");
    </script>
  </head>

  <body>
    <div class="debug"></div>
    <header>
      <!-- primera barra -->
      <nav
        class="navbar navbar-inverse navbar-fixed-top navbar-gob"
        role="navigation"
      >
        <div class="container">
          <div class="navbar-header">
            <button
              type="button"
              class="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#navbarGobCollapse"
              title="Interruptor de Navegación"
            ></button>
            <a class="navbar-brand" href="https://www.gob.mx/">
              <img
                src="https://framework-gb.cdn.gob.mx/landing/img/logoheader.svg"
                width="128"
                height="48"
                alt="Página de inicio, Gobierno de México"
              />
            </a>
          </div>
          <div class="collapse navbar-collapse show" id="navbarGobCollapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="https://www.gob.mx/tramites">Trámites</a></li>
              <li><a href="https://www.gob.mx/gobierno">Gobierno</a></li>
              <li>
                <a href="https://www.gob.mx/busqueda"
                  ><img
                    src="https://framework-gb.cdn.gob.mx/landing/img/lupa.png"
                /></a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <!-- tercera barra -->
      <nav
        class="navbar fixed-top navbar-expand-lg navbar-dark fixed-top container-fluid navbar-gaceta"
      >
        <div class="container">
          <button
            type="button"
            class="navbar-toggle collapsed"
            data-toggle="collapse"
            data-target="#navbarGaceta"
            title="Interruptor de Navegación"
          ></button>

          <!-- logo principal -->
          <div class="logo logoA">
            <a href="https://www.gob.mx/bachilleres" target="_blank"
              ><img src="../images/header/logoCB.svg"
            /></a>
          </div>

          <!-- menu -->
          <div class="collapse navbar-collapse" id="navbarGaceta">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="../index.php" title="Inicio"
                  >Inicio <span class="sr-only">(current)</span>
                </a>
              </li>
              <div class="separadormenu"></div>
              <li class="nav-item">
                <a class="nav-link" href="estudiantes.html" title="Estudiantes">Estudiantes</a>
              </li>
              <div class="separadormenu"></div>
              <li class="nav-item">
                <a class="nav-link" href="docentes.html" title="Docentes">Docentes</a>
              </li>
              <div class="separadormenu"></div>
              <li class="nav-item">
                <a class="nav-link" href="directores.html" title="Directivos escolares"
                  >Directivos escolares</a
                >
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">