<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Herramientas tecnológicas para la enseñanza 
        </h1>
      </div>
      <hr />
      <div class="container" style="margin-top: 50px;">
        <div class="row">
              <div class="col-md-12 text-center">
                <div>
                  <img src="docs/apoyo-psic-docentes/digitales.png" height="150" />
                </div>
                <div class="btn-group" role="group">
                  <button
                    id="btnGroupDrop1"
                    type="button"
                    class="btn dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    style="background-color: #00a8cc; color: #fff;"
                  >
                    Herramientas tecnológicas para la enseñanza
                  </button>
                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/TEAMS.pdf"
                      target="pdfreader"
                      >¿Qué hace TEAMS?</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/Teams-1Funcionalidades.pdf"
                      target="pdfreader"
                      >Funcionalidades de TEAMS</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/Teams-2Clases.pdf"
                      target="pdfreader"
                      >Clases desde cualquier lugar</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/Teams-3Caracteristicas.pdf"
                      target="pdfreader"
                      >Características de TEAMS</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/TeamsConferencias1.pdf"
                      target="pdfreader"
                      >Agenda reunión en TEAMS</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/TeamsConferencias2a.pdf"
                      target="pdfreader"
                      >Unirse a la videoconferencia TEAMS</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/TeamsConferencias2b.pdf"
                      target="pdfreader"
                      >Panel de conferencia en TEAMS</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/zoom.pdf"
                      target="pdfreader"
                      >Videoconferencias en ZOOM</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/face-academico.pdf"
                      target="pdfreader"
                      >Uso académico de FACEBOOK</a
                    >
                    <a 
                      class="dropdown-item"
                      href="docs/seguimiento-academico/digitales/Quizlet.pdf"
                      target="pdfreader"
                      >Fomenta el autoestudio y la autoevaluación</a
                    >
                    <!-- <a class="dropdown-item" href="#" target="pdfreader">Dropdown link</a> -->
                  </div>
                </div>
              </div>
        </div>
      </div>
      <br />
    </div>

    <div style="height: 350px;">&nbsp;</div>

    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>