<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-materiales.jpg" class="img-fluid" /> -->
        <h1 class="my-4">Mecanismos de Regularización</h1>
      </div>
      <hr />
      <br /><br />

      <div class="container">
        <div class="container align-items-center">
          <!-- Button trigger modal -->
          <!-- <button
            type="button"
            class="btn btn-primary"
            data-toggle="modal"
            data-target="#exampleModalCenter"
          >
            Aviso Importante
          </button> -->

          <!-- Modal -->
          <!-- <div
            class="modal fade bd-example-modal-lg"
            id="exampleModalCenter"
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div
              class="modal-dialog modal-dialog-centered modal-dialog modal-lg"
              role="document"
            >
              <div class="modal-content content-center">
                <div class="modal-header">
                  <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <img src="docs/AVISO-IMPORTANTE.jpg" class="img-modal" />
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <!-- info -->

        <div class="row mb-5">
          <div class="col-md-4 text-center">
            <div
              class="circulo"
              style="background-color: #919191; margin-bottom: 10px;"
            >
              <img src="docs/mecanismos/evarec.png" height="80" />
            </div>
            <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #df2b6a;"
                >
                  EVAREC en línea
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/plataforma_alumno.pdf"
                    target="pdfreader"
                    >Conoce la plataforma para<br />presentar tu examen en línea
                  </a>
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec/GUIA-INSCRIPCION.jpg"
                    target="pdfreader"
                    >Guía de inscripción</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec/mecanismoReg-linea.jpg"
                    target="pdfreader"
                    >Mecanismos de regularización</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec/calendario-EVAREC2020A.jpg"
                    target="pdfreader"
                    >Calendario EVAREC</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec/InfoPuntosClave.png"
                    target="pdfreader"
                    >Puntos clave para EVAREC en línea</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec/InfoRecomendaciones.png"
                    target="pdfreader"
                    >Recomendaciones para EVAREC</a
                  >
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div
              class="circulo"
              style="background-color: #f5d0a9; margin-bottom: 10px;"
            >
              <img src="docs/mecanismos/study.svg" height="80" />
            </div>
            <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #58acfa;"
                >
                  PAI en línea
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/pai/Conoce-PAI-2020A.pdf"
                    target="pdfreader"
                    >Conoce la plataforma</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/pai/PuntosClavePAI.png"
                    target="pdfreader"
                    >Puntos clave para PAI</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/pai/recomendacionesPAI.png"
                    target="pdfreader"
                    >Recomendaciones para el examen</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/pai/situacionesPAI.png"
                    target="pdfreader"
                    >Situaciones a considerar <br /> al presentar examen</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/pai/PortafolioEvidenciasOrientaciones.pdf"
                    target="pdfreader"
                    >Portafolio de Evidencias y orientaciones</a
                  >                  
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/pai/InfografiaPortafolio.jpg"
                    target="pdfreader"
                    >Inforgrafía Portafolio</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/pai/OrientacionesCursoPAI.pdf"
                    target="pdfreader"
                    >Orientaciones para preparar asesorias</a
                  >
              
              </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div
              class="circulo"
              style="background-color: #d4befc; margin-bottom: 10px;"
            >
              <img src="docs/mecanismos/mesa-ayuda.png" height="70" />
            </div>
            <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #c08fc1;"
                >
                  Mesa de ayuda PAI 2020-A en línea
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                  <!-- DIRECTORIO DE PAI -->
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_1_PAI.pdf"
                    target="pdfreader">MA Plantel 1 El Rosario</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_2_PAI.pdf"
                    target="pdfreader" >MA Plantel 2 Cien Metros</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_3_PAI.pdf"
                    target="pdfreader" >MA Plantel 3 Iztacalco</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_4_PAI.pdf"
                    target="pdfreader" >MA Plantel 4 Culhuacán</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_5_PAI.pdf"
                    target="pdfreader">MA Plantel 5 Satélite</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_6_PAI.pdf"
                    target="pdfreader">MA Plantel 6 Vicente Guerrero</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_7_PAI.pdf"
                    target="pdfreader">MA Plantel 7 Iztapalapa</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_8_PAI.pdf"
                    target="pdfreader">MA Plantel 8 Cuajimalpa</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_9_PAI.pdf"
                    target="pdfreader">MA Plantel 9 Aragón</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_10_PAI.pdf"
                    target="pdfreader">MA Plantel 10 Aeropuerto</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_11_PAI.pdf"
                    target="pdfreader">MA Plantel 11 Nueva Atzacoalco</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_12_PAI.pdf"
                    target="pdfreader">MA Plantel 12 Nezahualcóyotl</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_13_PAI.pdf"
                    target="pdfreader">MA Plantel 13 Xochimilco-Tepepan</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_14_PAI.pdf"
                    target="pdfreader">MA Plantel 14 Milpa Alta</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_15_PAI.pdf"
                    target="pdfreader">MA Plantel 15 Contreras</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_16_PAI.pdf"
                    target="pdfreader" >MA Plantel 16 Tláhuac</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_17_PAI.pdf"
                    target="pdfreader" >MA Plantel 17 Huayamilpas-Pedregal</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_18_PAI.pdf"
                    target="pdfreader">MA Plantel 18 Tlihuaca-Azcapotzalco</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_19_PAI.pdf"
                    target="pdfreader">MA Plantel 19 Ecatepec</a>
                  <a class="dropdown-item" href="docs/mecanismos/pai-mesa-ayuda/PLANTEL_20_PAI.pdf"
                    target="pdfreader">MA Plantel 20 Del Valle</a>

                  <!-- DIRECTORIO DE EVAREC
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL1_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 1 El Rosario</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL2_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 2 Cien Metros</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL3_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 3 Iztacalco</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL4_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 4 Culhuacán</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL5_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 5 Satélite</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL6_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 6 Vicente Guerrero</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL7_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 7 Iztapalapa</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL8_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 8 Cuajimalpa</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL9_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 9 Aragón</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL10_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 10 Aeropuerto</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL11_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 11 Nueva Atzacoalco</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL12_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 12 Nezahualcóyotl</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL13_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 13 Xochimilco-Tepepan</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL14_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 14 Milpa Alta</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL15_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 15 Contreras</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL14_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 16 Tláhuac</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL17_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 17 Huayamilpas-Pedregal</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL18_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 18 Tlihuaca-Azcapotzalco</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL19_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 19 Ecatepec</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/mecanismos/evarec-mesa-ayuda/PLANTEL20_MesaAyuda.pdf"
                    target="pdfreader"
                    >MA Plantel 20 Del Valle</a
                  >
                  -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- info -->
    </div>

    <div style="height: 650px;">&nbsp;</div>

    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>