<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <img src="../images/inicio-img/tira-directores.jpg" class="img-fluid" />
        <h1 class="my-4">Directivos escolares</h1>
      </div>
      <hr />

      <div class="container">
        <!-- info -->
        <div class="cajaSecciones">
          <ul>
            <li><a href="">Jefes de materia</a></li>
            <li>
              <a href="estrategias-seguimiento-plantel.php"
                >Estrategias de seguimiento académico por plantel</a
              >
            </li>
            <li><a href="">Formatos para el seguimiento académico </a></li>
            <li>
              <a
                href="https://www.gob.mx/bachilleres/articulos/marco-juridico-de-las-medidas-tomadas-por-el-colegio-
de-bachilleres-por-el-covid-19"
                target="_blank"
                >Documentos normativos</a
              >
            </li>
          </ul>
        </div>
      </div>
      <!-- info -->
    </div>
    <!-- fin Content -->

    <!-- Footer -->
 <?php include('footer.php');?>
