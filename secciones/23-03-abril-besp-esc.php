<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!-- <img src="docs/img-secciones/IMG_5450.JPG" class="img-fluid" /> -->
        <h1 class="my-4">
          Materiales de apoyo para las asignaturas (escolarizado)
        </h1>
      </div>
      <hr />
      <br />

      <div class="container text-center">
        <h2 class="my-4">
          Material para segundo corte
        </h2>
      </div>
      <br /><br />

      <div class="container">
        <div class="row">
          <div class="col-md-6 tablabasi-esp">
            <div class="fEspecifica">
              <h3>Formación básica y específica</h3>
            </div>
            <br />
            <div class="bgcaja">
              <h5 class="text-center" class="text-center">
                2<sup>o</sup> Semestre
              </h5>
              <ul>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/Activ-fisicas-deportivas-II.pdf"
                    target="_blank"
                    >Actividades Físicas y Deportivas II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/TIC-2.pdf"
                    target="_blank"
                    >TIC II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/QUIMICA-I.pdf"
                    target="_blank"
                    >Química I</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/MATEMATICAS-II.pdf"
                    target="_blank"
                    >Matemáticas II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/Lenguaje-y-comunicacion-II.pdf"
                    target="_blank"
                    >Lenguaje y comunicación II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/INGLES-II.pdf"
                    target="_blank"
                    >Inglés II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/FISICA-II.pdf"
                    target="_blank"
                    >Física II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/ETICA.pdf"
                    target="_blank"
                    >Ética</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/C-SOC-II.pdf"
                    target="_blank"
                    >Ciencias Sociales II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/2do-semestre/Aprec-artisca-II.pdf"
                    target="_blank"
                    >Apreciación Artística II</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">4<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/TIC-4.pdf"
                    target="_blank"
                    >TIC IV</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/Quimica-III.pdf"
                    target="_blank"
                    >Química III</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/Orientacion-II.pdf"
                    target="_blank"
                    >Orientación II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/Matematicas-IV.pdf"
                    target="_blank"
                    >Matemáticas IV</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/Lengua-y-literatura-II.pdf"
                    target="_blank"
                    >Lengua y literatura II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/Ingles-IV.pdf"
                    target="_blank"
                    >Inglés IV</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/HISTORIA-DE-MEXICO-II.pdf"
                    target="_blank"
                    >Historia de México II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/GEOGRAFIA-II.pdf"
                    target="_blank"
                    >Geografía II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/4to-semestre/BIOLOGIA-I.pdf"
                    target="_blank"
                    >Biología I</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">6<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre/TAP-II.pdf"
                    target="_blank"
                    >Taller de análisis y Producción de Textos II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre/PROBLEMAS-FILOSOFICOS.pdf"
                    target="_blank"
                    >Problemas Filósoficos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre/Ingles-VI.pdf"
                    target="_blank"
                    >Inglés VI</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre/Matematicas-VI.pdf"
                    target="_blank"
                    >Matemáticas VI</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre/ESEM-II.pdf"
                    target="_blank"
                    >Estructura Socioeconómica de México II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre/Ecologia.pdf"
                    target="_blank"
                    >Ecología</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">
                6<sup>o</sup> Semestre Formación Específica
              </h5>
              <ul>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/Salud-humana-II.pdf"
                    target="_blank"
                    >Salud Humana II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/Proy-Invers-II.pdf"
                    target="_blank"
                    >Proyectos de Inversión y Finanzas Personales II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/Proy-Gest-Soc-II.pdf"
                    target="_blank"
                    >Proyectos De Gestión Social II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/Int-Artistica%20II.pdf"
                    target="_blank"
                    >Interdisciplina Artística II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/Pro-industriales.pdf"
                    target="_blank"
                    >Procesos Industriales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/IngFis-II.pdf"
                    target="_blank"
                    >Ingeniería Física II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/Humanidades-II.pdf"
                    target="_blank"
                    >Humanidades II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/basica-especifica/6to-semestre-especifica/C-tecnologia-II.pdf"
                    target="_blank"
                    >Ciencia Y Tecnología II</a
                  >
                </li>
              </ul>
            </div>
          </div>
          <!--tabla1-->

          <div class="col-md-6 tablaLab">
            <div class="flaboral"><h3>Formación laboral</h3></div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">4<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/formacion-laboral/4to-semestre/Preparacion-de-alimentos-Orientaciones.docx.pdf"
                    target="_blank"
                    >Preparación de alimentos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/4to-semestre/Gestion-de-Personal_4o_Sem_Orientaciones.pdf"
                    target="_blank"
                    >Gestión de Personal</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/4to-semestre/4to_Crear_y_administrar_bases_de_datos.pdf"
                    target="_blank"
                    >Crear y Administrar Bases de Datos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/4to-semestre/Dibujo-de-Planos-AE_orientaciones4to_Semestre.docx.pdf"
                    target="_blank"
                    >Dibujo de Planos Arquitectónicos y Estructurales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/4to-semestre/Elaboracion-de-Estados-Financieros.pdf"
                    target="_blank"
                    >Elaboración de Estados Financieros</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/4to-semestre/4to_Correccion_y_edicion_fotografica.pdf"
                    target="_blank"
                    >Corrección y Edición Fotográfica</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/4to-semestre/4to_AFyQ_LQ_Orientaciones.pdf"
                    target="_blank"
                    >Análisis Físicos y Químicos</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">6<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/Proyecto-Integrador.pdf"
                    target="_blank"
                    >Proyecto Integrador</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/Prevencion-de-Riesgos-de-Trabajo.pdf"
                    target="_blank"
                    >Prevención de Riesgos de Trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/Integracion-de-proyectos.pdf"
                    target="_blank"
                    >Integración de proyectos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/Introduccion-al-Trabajo.pdf"
                    target="_blank"
                    >Introducción al Trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/Gestion-de-Calidad-de-un-Laboratorio.pdf"
                    target="_blank"
                    >Gestión de Calidad de un Laboratorio</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/Conservacion-de-Documentos-Orientaciones.pdf"
                    target="_blank"
                    >Conservación de Documentos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/Auditoria-nocturna-Orientaciones.pdf"
                    target="_blank"
                    >Auditoria nocturna</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/6to_Programacion_de_paginas_Web.pdf"
                    target="_blank"
                    >Programación de páginas Web</a
                  >
                </li>
                <li>
                  <a
                    href="docs/formacion-laboral/6to-semestre/6to_Diseno_en_2D_para_Web.pdf"
                    target="_blank"
                    >Diseño en 2D para Web</a
                  >
                </li>
              </ul>
            </div>
          </div>
          <!--tabla2-->
        </div>
      </div>
    </div>
    <!-- fin Content -->

   <?php include('footer.php');?>