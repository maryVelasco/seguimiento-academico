
<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>

    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
        <div class="container text-center">
          <h1 class="my-4">
           Recursos de apoyo escolarizado 2020-B 
          </h1>
        </div>
          <hr />
          <br /><br />
        
          <div class="row">
            <div class="col-md-6 centrar mb-4">
              <div class="text-center">
                  <div class="circulo" style="background-color: #e6e6e6;">
                    <img src="docs/img-secciones/pdf-downlodad.png" height="60" /> 
                  </div>
                  <h5>
                    <a href="docs/seguimiento-academico.pdf" target="_blank">
                    Seguimiento académico marco general semestre 2020-A </a>
                  </h5>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 centrar">
              <div class="text-center">
                <div class="circulo" style="background-color: #e6e6e6;">
                    <img src="docs/img-secciones/camara-video.png" height="60" /> 
                  </div>
                  <h5>
                    <a href="carpeta-experiencias.php" target="_blank">
                    ¿Cómo integro mi portafolio o carpeta de experiencias?</a>
                  </h5> 
              </div>
            </div>
          </div> 
    </div>
    <!-- fin Content -->
    <div style="height: 100px;"></div>

    <!-- Footer -->
<?php include('footer.php');?>

<style type="text/css">
  .tablaiconos td{ width:50%; text-align:center;}
  .centrar{margin: 0 auto;}

</style>