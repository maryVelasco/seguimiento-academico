<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">Programas de estudio: Orientaciones metodológicas</h1>
      </div>
      <hr />

      <div class="container">
        <div class="row">
          <div class="headsec1 col-md-12">
            <h4 class="py-2">2<sup>o</sup> Semestre</h4>
          </div>
          <div class="col-md-12 bdsec">
            <ul class="listmat">
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/programas-estudio/2do-semestre/Act-Fisicas-Deportivas-II.pdf"
                  target="_blank"
                  >Actividades Físicas y Deportivas II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/programas-estudio/2do-semestre/Ap-Art-II.pdf"
                  target="_blank"
                  >Apreciación Artística II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/programas-estudio/2do-semestre/Ciencias-soc-II.pdf"
                  target="_blank"
                  >Ciencias Sociales II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/programas-estudio/2do-semestre/Fisica-II.pdf"
                  target="_blank"
                  >Física II</a
                >
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>