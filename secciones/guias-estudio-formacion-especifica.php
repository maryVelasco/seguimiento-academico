<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
          <div class="container text-center">
            <!-- <img src="docs/img-secciones/tira-guias.jpg" class="img-fluid" /> -->
            <h1 class="my-4">
              Guías de estudio para formación básica y específica
            </h1>
            <hr />
            <br />
          </div>
      <!-- 
      <div class="container">
        <div class="row mb-4 ml-5">
          <img src="pdf.svg" height="30" class="mr-2" />
          <button type="button" class="btn btn-light">
            <a href="docs/seguimiento-academico.pdf" target="pdfreader" target="_blank"
              >Seguimiento académico marco general</a
            >
          </button>
        </div>
      </div> -->

      <div class="container"> <!-- primero y segundo -->
        <div class="row">
          <div class="col-md-6 tablabasi-esp">
            <div class="fEspecifica2">
              <h3>1° semestre</h3>
            </div>
            <br />
            <div class="bgcaja">
              <ul>
                <li>
                  <a
                      href="docs/guias/BE/1er-semestre/ActividadesFisicasyDeportivasI_20B.pdf"
                      target="pdfreader"
                      title="Actividades f&iacute;sicas y deportivas I">
                      Actividades f&iacute;sicas y deportivas I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/ApreciacionArtisticaI_20B.pdf"
                    target="pdfreader"
                    title="Apreciación artística I">
                    Apreciación artística I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/CSocI_20B.pdf"
                    target="pdfreader"
                    title="Ciencias sociales I">
                    Ciencias sociales I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/FisicaI_20B.pdf"
                    target="pdfreader"
                    title="Física I">
                    Física I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/InglesI_20B.pdf"
                    target="pdfreader"
                    title="Inglés I">Inglés I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/Intro_FilosofiaI_20B.pdf"
                    target="pdfreader"
                    title="Introducción a la Filosofía">Introducción a la filosofía</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/LenguajeyComunicacion1_20B.pdf"
                    target="pdfreader"
                    title="Lenguaje y Comunicación I">Lenguaje y comunicación I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/MateI_20B.pdf"
                    target="pdfreader"
                    title="Matemáticas I">Matemáticas I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/OrientacionI_20B.pdf"
                    target="pdfreader"
                    title="Orientación I">Orientación I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/1er-semestre/TIC1_20B.pdf"
                    target="pdfreader"
                    title="Tecnologías de la información y la comunicación I">Tecnologías de la información y la comunicación I</a>
                </li>
              </ul>
            </div>
          </div>

          <div class="col-md-6 tablabasi-esp">
            <div class="fEspecifica2">
              <h3>2° semestre</h3>
            </div>
            <br />
            <div class="bgcaja">
              <ul>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/actividades-fisicas-y-deportivas-II.pdf"
                    target="pdfreader"
                    title="Actividades f&iacute;sicas y deportivas II">
                  Actividades f&iacute;sicas y deportivas II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/apreciacion-artistica-II.pdf"
                    target="pdfreader"
                    title="Apreciación artística II">Apreciación artística II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/ciencias-sociales-II.pdf"
                    target="pdfreader"
                    title="Ciencias sociales II">Ciencias sociales II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/etica.pdf"
                    target="pdfreader"
                    title="Ética">Ética</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/fisica-II.pdf"
                    target="pdfreader"
                    title="Física II">Física II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/ingles-II.pdf"
                    target="pdfreader"
                    title="Inglés II">Inglés II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/lenguaje-y-comunicacion-II.pdf"
                    target="pdfreader"
                    title="Lenguaje y comunicación II">Lenguaje y comunicación II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/matematicas-II.pdf"
                    target="pdfreader"
                    title="Matemáticas II">Matemáticas II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/quimica-I.pdf"
                    target="pdfreader"
                    title="Química I">Química I</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/2do-semestre/tic-II.pdf"
                    target="pdfreader"
                    title="Tecnologías de la información y la comunicación II">Tecnologías de la información y la comunicación II</a>
                </li>
              </ul>
            </div>
          </div>
        </div> 
      </div> 
      <br />  
      <!--  --------------------------  SEMESTRES DEL PRIMER PERODO DEL AÑO     ---------------------- -->
        <div class="container"><!-- tercer y cuarto -->
          <div class="row">
            <div class="col-md-6 tablabasi-esp">
              <div class="fEspecifica2">
                <h3>3° semestre</h3>
              </div>
              <br />
              <div class="bgcaja">
                <ul>
                  <li>
                    <a
                      href="docs/guias/BE/3er-semestre/FisicaIII_20B.pdf"
                      target="pdfreader"
                      title="Física III">Física III</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/3er-semestre/Geografia I_20B.pdf"
                      target="pdfreader"
                      title="Geografía I">Geografía I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/3er-semestre/HistoriaMexicoI_20B.pdf"
                      target="pdfreader"
                      title="Historia de México I">Historia de México I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/3er-semestre/InglesIII_20B.pdf"
                      target="pdfreader"
                      title="Inglés III">Inglés III</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/3er-semestre/LenguayliteraturaI_20B.pdf"
                      target="pdfreader"
                      title="Lengua y Literatura I">Lengua y Literatura I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/3er-semestre/QuimicaII_20-B.pdf"
                      target="pdfreader"
                      title="Química II">Química II</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/3er-semestre/TIC3_20B.pdf"
                      target="pdfreader"
                      title="Tecnologías de la Información y la comunicación III">Tecnologías de la Información y la comunicación III</a>
                  </li> 
                </ul>
              </div>
            </div>

            <div class="col-md-6 tablabasi-esp">
              <div class="fEspecifica2">
                <h3>4° semestre</h3>
              </div>
              <br />
              <div class="bgcaja">
                <ul>
                  <li>
                  <a
                    href="docs/guias/BE/4to-semestre/biologia-I.pdf"
                    target="pdfreader"
                    title="Biología I">Biología I</a>
                </li>
                  <li>
                  <a
                    href="docs/guias/BE/4to-semestre/geografia-II.pdf"
                    target="pdfreader"
                    title="Geografía II">Geografía II</a>
                </li>
                  <li>
                  <a
                    href="docs/guias/BE/4to-semestre/historia-de-mexico-II.pdf"
                    target="pdfreader"
                    title="Historia de México II">Historia de México II</a>
                </li>
                  <li>
                  <a
                    href="docs/guias/BE/4to-semestre/ingles-IV.pdf"
                    target="pdfreader"
                    title="Ingles IV">Ingles IV</a>
                </li>
                  <li>
                  <a
                    href="docs/guias/BE/4to-semestre/literatura-II.pdf"
                    target="pdfreader"
                    title="Lengua y literatura II">Lengua y literatura II</a>
                </li>
                  <li>
                  <a
                    href="docs/guias/BE/4to-semestre/matematicas-IV.pdf"
                    target="pdfreader"
                    title="Matemáticas IV">Matemáticas IV</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/4to-semestre/orientacion-II.pdf"
                    target="pdfreader"
                    title="Orientación II">Orientación II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/4to-semestre/quimica-III.pdf"
                    target="pdfreader"
                    title="Guía de química III">Guía de química III</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/4to-semestre/tic-IV.pdf"
                    target="pdfreader"
                    title="Tecnologías de la información y la comunicación IV">Tecnologías de la información y la comunicación IV</a>
                </li>
                </ul>
              </div>
              <br />
            </div>
          </div>
        </div>
        <br />

<!--  --------------------------  SEMESTRES DEL PRIMER PERODO DEL AÑO     ---------------------- -->
        <div class="container"><!-- quinto y sexto -->
          <div class="row">
            <div class="col-md-6 tablabasi-esp">
              <div class="fEspecifica2">
                <h3>5° semestre</h3>
              </div>
              <br />
              <div class="bgcaja">
                <ul>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/BiologiaII_20B.pdf"
                      target="pdfreader"
                      title="Biología II">Biología II</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/CienciayTecnologiaI_20B.pdf"
                      target="pdfreader"
                      title="Ciencia y tecnología I">Ciencia y tecnología I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/HumanidadesI_20B.pdf"
                      target="pdfreader"
                      title="Humanidades I">Humanidades I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/IngenieriaFisicaI_20B.pdf"
                      target="pdfreader"
                      title="Ingeniería física I">Ingeniería física I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/InglesV_20B.pdf"
                      target="pdfreader"
                      title="Inglés V">Inglés V</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/InterdisciplinaArtisticaI_20B.pdf"
                      target="pdfreader"
                      title="Interdisciplina artística I">Interdisciplina artística I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/Logicayargumentacion_20B.pdf"
                      target="pdfreader"
                      title="Lógica y argumentación">Lógica y argumentación</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/MateV_20B.pdf"
                      target="pdfreader"
                      title="Matemáticas V">Matemáticas V</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/ProyectosdeGestionSocial I_20B.pdf"
                      target="pdfreader"
                      title="Guía de estudios de proyectos de gestión social">Guía de estudios de proyectos de gestión social</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/ProyectosdeInversionyFinanzasPersonales I_20B.pdf"
                      target="pdfreader"
                      title="Proyectos de inversión y finanzas personales I">Proyectos de inversión y finanzas personales I</a>
                  </li>
                  <li>
                    <a
                      href="docs/guias/BE/5to-semestre/QuimicadelCarbono_20B.pdf"
                      target="pdfreader"
                      title="Química del carbono">Química del carbono</a>
                  </li>
                   <a
                      href="docs/guias/BE/5to-semestre/SaludHumanaI_20B.pdf"
                      target="pdfreader"
                      title="Salud humana I">Salud humana I</a>
                  </li>
                   </li>
                   <a
                      href="docs/guias/BE/5to-semestre/TAPTI_20B.pdf"
                      target="pdfreader"
                      title="Taller de análisis y producción de textos I">Taller de análisis y producción de textos I</a>
                  </li>
                 
                </ul>
              </div>
            </div>

            <div class="col-md-6 tablabasi-esp">
              <div class="fEspecifica2">
                <h3 class="text-center">6<sup>o</sup> Semestre</h3>
              </div>
              <br />
              <div class="bgcaja">
                <ul>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/ciencia-y-tecnologia-II.pdf"
                    target="pdfreader"
                    title="Ciencia y tecnología II">Ciencia y tecnología II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/ecologia-I.pdf"
                    target="pdfreader"
                    title="Ecología"
                    >Ecología</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/esem-II.pdf"
                    target="pdfreader"
                    title="Estructura socioeconómica de México II"
                    >Estructura socioeconómica de México II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/humanidades-II.pdf"
                    target="pdfreader"
                    title="Humanidades II"
                    >Humanidades II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/ingenieria-fisica-II.pdf"
                    target="pdfreader"
                    title="Ingeniería Física II"
                    >Ingeniería Física II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/ingles-VI.pdf"
                    target="pdfreader"
                    title="Inglés VI"
                    >Inglés VI</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/interdisciplina-artistica-II.pdf"
                    target="pdfreader"
                    title="Interdisciplina artística II"
                    >Interdisciplina artística II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/matematicas-VI.pdf"
                    target="pdfreader"
                    title="Matemáticas VI"
                    >Matemáticas VI</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/problemas-filosoficos.pdf"
                    target="pdfreader"
                    title="Problemas filosóficos"
                    >Problemas filosóficos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/procesos-industriales.pdf"
                    target="pdfreader"
                    title="Procesos industriales"
                    >Procesos industriales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/proyectos-de-gestion-social-II.pdf"
                    target="pdfreader"
                    title="Proyectos de gestión social II"
                    >Proyectos de gestión social II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/proyectos-de-inversion-II.pdf"
                    target="pdfreader"
                    title="Proyectos de inversión y finanzas personales II"
                    >Proyectos de inversión y finanzas personales II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/salud-humana-II.pdf"
                    target="pdfreader"
                    title="Salud humana II"
                    >Salud humana II</a>
                </li>
                <li>
                  <a
                    href="docs/guias/BE/6to-semestre/tapt-II.pdf"
                    target="pdfreader"
                    title="Taller de análisis y producción de textos II"
                    >Taller de análisis y producción de textos II</a>
                </li>
              </ul>
              </div>
              <br />
            </div>
          </div>
        </div>
        <br />
<!--  -------------------------- FIN SEMESTRES DEL PRIMER PERODO DEL AÑO     ---------------------- -->

    </div>
    <!-- fin Content -->

    <!-- Footer -->
   <?php include('footer.php');?>
