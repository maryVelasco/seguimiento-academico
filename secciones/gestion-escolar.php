<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-materiales.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Gestión escolar
        </h1>
      </div>
      <hr />

      <div class="container" style="margin-top: 50px;">

        <div class="row"> <!--fila pdf------------------------ --> 
  <!-- ------------------------------------------------- --> 
          <div class="col-md-3 text-center mb-5">
            <a href="docs/gestion-escolar/1_como_me_organizo.pdf"
            title="Estrategia para identificar lo importante y lo urgente de las actividades escolares con el objetivo de encontrar la mejor manera de priorizar las actividades." target="_blank">
            <img src="docs/img-secciones/pdf-downlodad.png" width="60"></a>
            <h5>¿Muchas cosas por hacer? Organiza tus prioridades </h5>
           <!--  <div>
              <img src="docs/apoyo-psic-docentes/oferta.png" height="150" />
            </div>
            <div class="btn-group" role="group">
              <button
                id="btnGroupDrop1"
                type="button"
                class="btn dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style="background-color: #ff9234; color: #fff;"
              >
                Oferta institucional
              </button>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a
                  class="dropdown-item"
                  href="https://view.genial.ly/5ede8b4cadf6170d8aac5bab"
                  target="_blank"
                  >Oferta de formación<br />
                  y actualización 2020B</a
                >
              </div>
            </div> -->
          </div>

          <div class="col-md-3 text-center mb-5">
            <a href="docs/gestion-escolar/2_no_presencial.pdf"
            title="
              16 recomendaciones organizadas bajo tres temáticas:  
              - Adaptar la planeación a la modalidad no presencial. 
              - Acompañar a los estudiantes en la modalidad no presencial.  
              - Desarrollo personal y profesional como docente en este nuevo contexto." 
            target="_blank">
            <img src="docs/img-secciones/pdf-downlodad.png" width="60"></a>
            <h5>Transición a la docencia no presencial </h5>
          </div>

          <div class="col-md-3 text-center mb-5">
            <a href="docs/gestion-escolar/3_taller_mejoredu.pdf"
            title="
              6 módulo para fortalecer el liderazgo del directivo: 
              - Liderazgo directivo 
              - Gestión y contingencia 
              - Estudiantes: igualdad y diversidad 
              - Colaboración docente 
              - Implicación de padres de familia 
              - Condiciones para un nuevo comienzo" 
            target="_blank">
            <img src="docs/img-secciones/pdf-downlodad.png" width="60"></a>
            <h5>Transición a la docencia no presencial </h5>
          </div>

          <div class="col-md-3 text-center mb-5">
            <a href="docs/gestion-escolar/4_propuestas_uchile.pdf"
            title="
              Recomendaciones presentadas en tres áreas:  
              1. Bienestar de las comunidades escolares 
              2. Priorización curricular, ajustes evaluativos y de gestión 
              3. Educación remota " 
            target="_blank">
            <img src="docs/img-secciones/pdf-downlodad.png" width="60"></a>
            <h5>Propuestas en bienestar de las comunidades escolares</h5>
          </div>
<!-- ------------------------------------------------- --> 

        <div class="row"> <!--fila desplegables------------------------ --> 
  <!-- ------------------------------------------------- -->        
          <div class="col-md-4 text-center">
            <!-- <div>
              <img src="docs/" height="150" title="Se identifican los principales desafíos ante la contingencia y se ofrecen propuestas para resolverlos." />
            </div> -->
            <div class="btn-group" role="group">
              <button
                id="btnGroupDrop1"
                type="button"
                class="btn dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style="background-color: #ff9234; color: #fff;"
                title="">
                Equipos de conducción<br> frente al Covid 
              </button>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="docs/gestion-escolar/5_conduccion_covid_1.pdf" target="_blank" data-toggle="tooltip" data-placement="right"  data-html="true" 
                    title="
                      - ¿Cómo redefinir los proyectos institucionales?<br><br>
                      - ¿Cómo capitalizar la potencia de los diferentes actores?<br><br>
                      - ¿Qué, cómo, cuándo y a quién comunicar las diferentes decisiones y líneas de trabajo?<br><br>
                      - ¿Cómo favorecer y acompañar el trabajo de los equipos docentes en la coyuntura?<br><br>
                      - ¿Cuál es el rol de las tecnologías para seguir educando en la emergencia?">
                5_conduccion_covid_1.pdf</a>
                <a class="dropdown-item" href="docs/gestion-escolar/5_conduccion_covid_2.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                title="
                      - ¿Cómo acompañar al equipo docente y orientarlo para dar seguimiento a las familias y las y los estudiantes?<br><br>
                      - ¿Qué posibilidades existen de llegar a las y los estudiantes y sus familias para mantener la cercanía?<br><br> 
                      - ¿Por qué es importante el seguimiento al equipo docente y a la situación de las y los estudiantes y sus familias?<br><br>  
                      - Familias a distancia de la escuela vs. la escuela adentro de la casa. ¿Nuevas alianzas? ¿Reformulando los vínculos?<br><br>  
                      - ¿En qué otros actores hay que pensar para dar respuesta a las situaciones adversas de las familias?">
                5_conduccion_covid_2.pdf</a>
                <a class="dropdown-item" href="docs/gestion-escolar/5_conduccion_covid_3.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                   title="
                      - ¿Cómo lograr proximidad afectiva bajo condiciones de distancia física?<br><br>  
                      - ¿Cómo contener emocionalmente en contexto de pandemia?<br><br>  
                      - ¿Cuál es la importancia de que se aborden los sentimientos?<br><br>  
                      - ¿Qué estrategias se pueden proponer para que se expresen las emociones?<br><br> 
                      - ¿Qué estrategias se pueden proponer para tramitar las emociones en la vuelta a las aulas? ">
                5_conduccion_covid_3.pdf</a>
                <a class="dropdown-item" href="docs/gestion-escolar/5_conduccion_covid_4.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                   title="
                      - ¿Cómo evidenciar que las y los estudiantes hacen tareas y están aprendiendo?<br><br>   
                      - ¿Por qué y para qué ofrecer retroalimentaciones a docentes, familias y estudiantes?<br><br>
                      - ¿Cómo ofrecer retroalimentaciones que acompañen el desarrollo de saberes y habilidades?<br><br>  
                      - ¿Cómo organizar las retroalimentaciones?<br><br>  
                      - ¿Cómo involucrar a docentes y estudiantes en su propio proceso de retroalimentación? ">
                5_conduccion_covid_4.pdf</a>
                <a class="dropdown-item" href="docs/gestion-escolar/5_conduccion_covid_5.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                   title="
                      - ¿Es posible seguir enseñando en tiempos de pandemia?<br><br>   
                      - ¿Se están reinventando las prácticas de la enseñanza en el contexto de una pandemia?<br><br> 
                      - ¿Cuál es el rol del trabajo colectivo en la reinvención de las prácticas de la enseñanza?<br><br>    
                      - ¿De qué formas las TIC ayudan a sostener propuestas de enseñanza renovadas?<br><br>   
                      - ¿Cómo sostener al estudiantado hoy y cómo hacerlo si la crisis se prolonga?  ">
                5_conduccion_covid_5.pdf</a>
                <a class="dropdown-item" href="docs/gestion-escolar/5_conduccion_covid_6.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                   title="
                      - La salud y el reencuentro: ¿cómo reconstruir el sentido de la escuela como un “lugar seguro”?<br><br> 
                      - Los equipos y el reencuentro: ¿cómo nos preparamos para recibir a la comunidad educativa?<br><br>  
                      - Las emociones y el reencuentro: ¿cómo elaborar las experiencias y las emociones de despierta el “salir de casa”?<br><br> 
                      - Los aprendizajes y el reencuentro: ¿cómo resignificar los aprendizajes construidos en la educación a distancia?<br><br> 
                      La comunidad y el reencuentro: ¿cómo potenciar la relación entre escuela y comunidad? ">
                5_conduccion_covid_6.pdf</a>
              </div>
            </div>
          </div>

          <div class="col-md-4 text-center">
          </div>

          <div class="col-md-4 text-center">
            <!-- <div>
              <img src="docs/apoyo-psic-docentes/actualizacion.png" height="150"
                   data-title="Recomendaciones para hacer de los procesos educativos a distancia una oportunidad"/>
            </div> -->
            <div class="btn-group" role="group">
              <button
                id="btnGroupDrop1"
                type="button"
                class="btn dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style="background-color: #7c3c21; color: #fff;"
              >
                 Educar en tiempos <br> de pandemia 
              </button>
              <div class="dropdown-menu menu_morgan" aria-labelledby="btnGroupDrop1">
                <ul>
                      <li>
                        <a class="dropdown-item" href="docs/gestion-escolar/6_educar_pandemia_1.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                            title="
                            Recomendaciones presentadas en 7 temas.<br><br>
                            - Tomar conciencia de la situación de los estudiantes.<br> 
                            - Acompañar a las familias.<br> 
                            - Apuntar al aprendizaje profundo.<br>  
                            - Distinguir lo esencial del currículo.<br>  
                            - Seleccionar las experiencias mínimas necesarias.<br>  
                            - Monitorear a las y los estudiantes.<br>
                            - Sugerencia de secuencias metodológicas para generar experiencias de aprendizaje ">
                          6_educar_pandemia_1.pdf </a></li>
                      <li>
                        <a class="dropdown-item" href="docs/gestion-escolar/6_educar_pandemia_2.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                            title="
                            Recomendaciones presentadas en 7 temas.<br><br> 
                            - Fortalecer los vínculos.<br>  
                            - Avanzar en una cultura del cuidado mutuo.<br>  
                            - Evitar la sobrecarga. <br> 
                            - Pedagogizar las emociones.<br>  
                            - Potenciar el desarrollo profesional.<br>  
                            - Vincular ejercicio ciudadano y educación socioemocional.<br>  
                            - Utilizar y ampliar las redes de apoyo.">
                          6_educar_pandemia_2.pdf </a></li>
                      <li>
                        <a class="dropdown-item" href="docs/gestion-escolar/6_educar_pandemia_3.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                            title="
                            Recomendaciones presentadas en 7 temas.<br><br> 
                            - Evaluar más, calificar menos.<br>  
                            - Ajustar el reglamento de evaluación haciendo uso del Decreto 67/2018 [No aplica para México]<br>   
                            - Promover el trabajo colaborativo docente.<br>  
                            - Coordinar el sistema de monitoreo de estudiantes.<br>   
                            - Definir qué evaluarán en contexto de educación a distancia.<br>   
                            - Definir cómo evaluar en contexto de educación a distancia.<br>   
                            - Fomentar la creación de experiencias evaluativas y compartirlas.">
                          6_educar_pandemia_3.pdf </a></li>
                      <li>
                        <a class="dropdown-item" href="docs/gestion-escolar/6_educar_pandemia_4.pdf" target="_blank" data-toggle="tooltip" data-placement="right" data-html="true"
                            title="
                            Recomendaciones presentadas en 7 temas.<br><br> 
                            - Fomentar una cultura colaborativa para la valoración de la diversidad.<br>  
                            - Dar relevancia a los principios del Diseño Universal del aprendizaje [No aplica para México] <br> 
                            - Fortalecer el vínculo familia-escuela.<br>  
                            - Generar sistema de acompañamiento pedagógico a distancia.<br>  
                            - Diversificar estrategias y herramientas.<br>  
                            - Evaluar más y calificar menos<br>
                            - Activar y potenciar redes de apoyo para la gestión de la diversidad">
                          6_educar_pandemia_4.pdf </a></li>
                </ul>
              </div>
            </div>
          </div>
  <!-- ------------------------------------------------- -->
        </div>
      </div>
      <br />
    </div>

    <style type="text/css">
      .menu_morgan{}
      .menu_morgan ul{
        list-style-type: none;
        padding: 0;
      }
      .menu_morgan ul li{}
      .menu_morgan ul li label{
          display: block;
          width: 100%;
          padding: 0.25rem 1.5rem;
          clear: both;
          font-weight: 400;
          color: #212529;
          text-align: inherit;
          white-space: nowrap;
          background-color: transparent;
          border: 0;
          margin: 0;
      }
      .menu_morgan ul li a{}
      .menu_morgan ul li a:hover{
        background-color: #e8e9ea;
      }
      .menu_morgan ul li label:hover{
        background-color: #e8e9ea;
      }
      .menu_morgan ul ul {
          display: none;
          border: 1px solid;
          position: absolute;
          background-color: #ccc;
          margin-left: -200px;
          margin-top: -30px;
      }
      .menu_morgan ul ul.derecha{
          margin-left: 200px;
          margin-top: -30px;
      }
      .menu_morgan ul ul li{}
      .menu_morgan ul li:hover ul{
        display: block;
      }

      .tooltip{}
      .tooltip .tooltip-inner{
          text-align: left;
          padding: 1rem;
      }
      .lista-tooltip{}
      .lista-tooltip li{list-style-type:none;}

        [data-title]:hover:after {
        opacity: 1;
        transition: all 0.1s ease 0.5s;
        visibility: visible;
    }

    [data-title]:after {
        content: attr(data-title);
        background-color: #81F7D8;
    }

    </style>

    <div style="height: 350px;">&nbsp;</div>

    <!-- fin Content -->

    <!-- Footer -->

<?php include('footer.php');?>
