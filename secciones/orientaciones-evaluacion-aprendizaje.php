<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!--  <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Evaluación de los aprendizajes
        </h1>
      </div>
      <hr />

      <div class="container">
        <div class="row">
          <div class="col-md-12 bdsec2">
            <ul class="listmat5">
              <li class="mb-3">
                <img src="pdf-bco.svg" height="30" /><a
                  href="docs/Orienta-EvalAprendizajes.pdf"
                  target="_blank"
                >
                 Evaluación de los aprendizajes</a
                >
              </li>
              <li class="mb-3">
                <img src="pdf-bco.svg" height="30" /><a
                  href="docs/seguimiento/GuiaReactivos-opcionMultiple.pdf"
                  target="_blank"
                >
                  Guía para elaborar reactivos de opción múltiple</a
                >
              </li>
              <li class="mb-3">
                <img src="pdf-bco.svg" height="30" /><a
                  href="docs/seguimiento/Instrumentos-evaluacion.pdf"
                  target="_blank"
                >
                  Instrumentos para la evaluación del aprendizaje: Escalas</a
                >
              </li>
              <li class="mb-3">
                <img src="pdf-bco.svg" height="30" /><a
                  href="docs/seguimiento/Referentesevaluacion.pdf"
                  target="_blank"
                >
                  Referentes para la planeación y la evaluación de los
                  aprendizajes en el Colegio de Bachilleres
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <br /><br /><br /><br /><br /><br />
    </div>
    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>
