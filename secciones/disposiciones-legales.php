<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4 container_gral">
      <div class="container text-center">
        <!-- <img src="docs/img-secciones/IMG_5450.JPG" class="img-fluid" /> -->
        <h1 class="my-4">Disposiciones legales por contingencia</h1>
        <hr />
        <br /><br />
      </div>

      <div class="container">
        <!-- planteles <-->

        <div class="row">
          <table class="table table-sm table-striped table-bordered">
            <thead></thead>
            <tbody>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://www.gob.mx/cms/uploads/attachment/file/552571/ACUE
RDO_ASF_amplia_suspension_mayo_15.pdf"
                    target="_blank"
                  >
                    ACUERDO de ampliación de la suspensión de los plazos y
                    términos legales en la Auditoría Superior
                  </a>
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://www.gob.mx/cms/uploads/attachment/file/552572/ACUE
RDO_INAI_amplia_periodo_de_carga_de_informacion_mayo_13.
pdf"
                    target="_blank"
                  >
                    ACUERDO aprueba modificación a los relativos al periodo de
                    la carga de información de transparencia
                  </a>
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://www.gob.mx/cms/uploads/attachment/file/552578/ACUE
RDO_SEP_suspension_plazos_y_terminos_mayo_18.pdf"
                    target="_blank"
                  >
                    Acuerdo suspenden los plazos y términos relacionados con
                    trámites y procedimientos
                  </a>
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://www.gob.mx/cms/uploads/attachment/file/552575/ACUE
RDO_SS_modifica_reactivacion_y_semaforo_mayo_15.pdf"
                    target="_blank"
                  >
                    Acuerdo estrategia para la reapertura de las actividades
                    sociales, educativas y económicas y sistema de semáforo por
                    regiones
                  </a>
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://www.gob.mx/cms/uploads/attachment/file/552574/ACUE
RDO_SS_estrategia_reapertura.pdf"
                    target="_blank"
                  >
                    ACUERDO estrategia para reapertura de las actividades
                    sociales, educativas y económicas, y sistema de semáforo por
                    regiones por semana
                  </a>
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/ACUERDO-TFJA-guardias%20temporales.pdf"
                    target="_blank"
                  >
                    ACUERDO TFJA guardias temporales
                  </a>
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/ACUERDO-SFP-
suspension-plazoa-1804-1705.pdf"
                    target="_blank"
                  >
                    ACUERDO por el que se suspenden plazos y términos legales</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/ACUERDO-
SEPsuspension-plz-teminos-2303-1805.pdf"
                    target="_blank"
                  >
                    ACUERDO número 07/04/20 por el que se modifica el diverso</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/ACUERDO-ASF-amplia-
suspension.pdf"
                    target="_blank"
                  >
                    ACUERDO suspensión de los plazos y términos Auditoría
                    Superior</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/ACUERDO-STPS-
modifica-suspension-ST.pdf"
                    target="_blank"
                  >
                    ACUERDO suspensión de los plazos y términos Secretaría del
                    Trabajo</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/ACUERDO-SFP-
lineamientos-intercambio1704.pdf"
                    target="_blank"
                  >
                    ACUERDO por el que se establecen los Lineamientos para
                    intercambio</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="https://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/ACUERDO-CJ6_2020MedidasContingencia.pdf"
                    target="_blank"
                  >
                    ACUERDO General 6/2020 del Pleno del Consejo de la
                    Judicatura Federal ...</a
                  >
                </th>
              </tr>

              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/02SEPAcuerdoPlazos.pdf"
                    target="_blank"
                  >
                    ACUERDO número 03/03/20 por el que se suspenden los plazos
                  </a>
                </th>
              </tr>

              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/03Consejo.pdf"
                    target="_blank"
                  >
                    ACUERDO por el que el Consejo de Salubridad General</a
                  >
                </th>
              </tr>

              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/04SFPAcuerdoMARZO23.pdf"
                    target="_blank"
                  >
                    ACUERDO por el que se establecen los criterios en materia de
                    administración de recursos humanos</a
                  >
                </th>
              </tr>

              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/05SAcuerdo.pdf"
                    target="_blank"
                  >
                    ACUERDO por el que se establecen las medidas preventivas</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/06SEPAmpliaPeriodo.pdf"
                    target="_blank"
                  >
                    ACUERDO número 06/03/20 por el que se amplía el periodo
                    suspensivo del 27 de marzo al 30 de abril</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/COLBACH01Com-16mar.pdf"
                    target="_blank"
                  >
                    El Colegio de Bachilleres informa</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/COLBACH02Com-medidas.pdf"
                    target="_blank"
                  >
                    Medidas a adoptar para la semana del 17 al 20 de marzo</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/COLBACH03Comuo19mar.pdf"
                    target="_blank"
                  >
                    Medidas para atender la contingencia</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/COLBACH04Com-2abril-Ampliacion.pdf"
                    target="_blank"
                  >
                    Comunicado del Director General</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/SuspensionPlazosTerminos.pdf"
                    target="_blank"
                  >
                    ACUERDO SEP Suspensión plazos y términos</a
                  >
                </th>
              </tr>
              <tr>
                <th>
                  <img
                    src="../images/notas-img/descargar.svg"
                    style="height: 20px;"
                  />
                </th>
                <th>
                  <a
                    href="http://cbgobmx.cbachilleres.edu.mx/blog-notas/2020/legalidad-contingencia/intercambio-informacion.pdf"
                    target="_blank"
                  >
                    ACUERDO SFP lineamientos para el intercambio de
                    información</a
                  >
                </th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-- fin Content -->
    </div>

    <!-- Footer -->
<?php include('footer.php');?>
