<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
   
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">Programas de estudio: Orientaciones metodológicas</h1>
      </div>
      <hr />

      <div class="container">
        <div class="row">
          <div class="headsec1 col-md-12">
            <h4 class="py-2">6<sup>o</sup> Semestre</h4>
          </div>
          <div class="col-md-12 bdsec">
            <ul class="listmat">
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/ciencia-y-tecnologia-II.pdf"
                  target="_blank"
                  >Ciencia y Tecnolog&iacute;a II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/ecologia.pdf"
                  target="_blank"
                  >Ecolog&iacute;a</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/ESEMII.pdf"
                  target="_blank"
                  >Estructura Socioeconómica de México II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/humanidades-II.pdf"
                  target="_blank"
                  >Humanidades II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/ingenieria-fisica-II.pdf"
                  target="_blank"
                  >Ingenieria F&iacute;sica</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/interdisciplina-artistica-II.pdf"
                  target="_blank"
                  >Interdisciplina Artistica II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/matematicas-VI.pdf"
                  target="_blank"
                  >Matem&aacute;ticas VI</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/problemas-filosoficos.pdf"
                  target="_blank"
                  >Problemas Filosóficos
                </a>
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/procesos-industriales.pdf"
                  target="_blank"
                  >Procesos Industriales</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/proyectos-de-gestion-social-II.pdf"
                  target="_blank"
                  >Proyectos de Gesti&oacute;n Social II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/proyectos-de-inversion-y-finanzas-personales.pdf"
                  target="_blank"
                  >Proyectos de Inversi&oacute;n y Finanzas Personales II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/salud-humana-II.pdf"
                  target="_blank"
                  >Salud Humana II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/6to-semestre-Or-Met/TAPT-II.pdf"
                  target="_blank"
                  >Taller de Análisis y Producción de Textos II</a
                >
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- fin Content -->

   <?php include('footer.php');?>
