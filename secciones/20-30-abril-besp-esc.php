<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <h1 class="my-4">
          Materiales de apoyo para las asignaturas (escolarizado)
        </h1>
      </div>
      <hr />
      <br />

      <div class="container text-center">
        <h2 class="my-4">
          Material para tercer corte
        </h2>
      </div>
      <br /><br />

      <div class="container">
        <div class="row">
          <div class="col-md-6 tablabasi-esp">
            <div class="fEspecifica">
              <h3>Formación básica y específica</h3>
            </div>
            <br />
            <div class="bgcaja">
              <h5 class="text-center" class="text-center">
                2<sup>o</sup> Semestre
              </h5>
              <ul>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/Act_fisicas_y_deportivas-II.pdf"
                    target="_blank"
                    >Actividades Físicas y Deportivas II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/Apreciacion_Artistica_II.pdf"
                    target="_blank"
                    >Apreciación Artística II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/Ciencias-Sociales-II.pdf"
                    target="_blank"
                    >Ciencias Sociales II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/ETICA.pdf"
                    target="_blank"
                    >Ética</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/FISICA-II.pdf"
                    target="_blank"
                    >Física II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/Ingles-II.pdf"
                    target="_blank"
                    >Inglés II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/Lenguaje-Comunicacion-II.pdf"
                    target="_blank"
                    >Lenguaje y comunicación II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/Matematicas-II.pdf"
                    target="_blank"
                    >Matemáticas II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/Quimica-I.pdf"
                    target="_blank"
                    >Química I</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/2do-semestre/TIC2.pdf"
                    target="_blank"
                    >TIC II</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">4<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/Biologia-I.pdf"
                    target="_blank"
                    >Biología I</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/geografia-II.pdf"
                    target="_blank"
                    >Geografía II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/historia-mexico-II.pdf"
                    target="_blank"
                    >Historia de México II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/ingles-IV.pdf"
                    target="_blank"
                    >Inglés IV</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/lengua-literatura-II.pdf"
                    target="_blank"
                    >Lengua y literatura II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/matematicas-IV.pdf"
                    target="_blank"
                    >Matemáticas IV</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/orientacion-II.pdf"
                    target="_blank"
                    >Orientación II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/quimica-III.pdf"
                    target="_blank"
                    >Química III</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/4to-semestre/TIC4.pdf"
                    target="_blank"
                    >TIC IV</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">6<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/"
                    target="_blank"
                  ></a
                  >Ciencia y Tecnología II
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/ecologia-contingencia.pdf"
                    target="_blank"
                    >Ecología</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/est-socioeconomica.pdf"
                    target="_blank"
                    >Estructura Socioeconómica de México II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/humanidades-II.pdf"
                    target="_blank"
                    >Humanidades II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/ingenieria-fisica-II.pdf"
                    target="_blank"
                    >Ingeniería Física II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/ingles-VI.pdf"
                    target="_blank"
                    >Inglés VI</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/Interdisc_Artistica_II.pdf"
                    target="_blank"
                    >Interdisciplina Artística II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/Matematicas-VI.pdf"
                    target="_blank"
                    >Matemáticas VI</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/problemas-filosoficos.pdf"
                    target="_blank"
                    >Problemas Filósoficos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/Procesos_industriales.pdf"
                    target="_blank"
                    >Procesos Industriales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/Proyec_Gestion_Soc_II.pdf"
                    target="_blank"
                    >Proyectos de Gestión Social II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/Proyec_Inversion_II.pdf"
                    target="_blank"
                    >Proyectos de Inversión y Finanzas Personales II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/salud-humana-II.pdf"
                    target="_blank"
                    >Salud Humana II</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30-abril-BESC/6to-semestre/taller-analisis-textos-II.pdf"
                    target="_blank"
                    >Taller de análisis y Producción de Textos II</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <!-- <div class="bgcaja">
       <h5 class="text-center">6<sup>o</sup> Semestre Formación Específica</h5>
        <ul>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/Salud-humana-II.pdf"target="_blank">Salud Humana II</a></li>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/Proy-Invers-II.pdf"target="_blank">Proyectos de Inversión y Finanzas Personales II</a></li>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/Proy-Gest-Soc-II.pdf"target="_blank">Proyectos De Gestión Social II</a></li>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/Int-Artistica%20II.pdf"target="_blank">Interdisciplina Artística II</a></li>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/Pro-industriales.pdf"target="_blank">Procesos Industriales</a></li>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/IngFis-II.pdf"target="_blank">Ingeniería Física II</a></li>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/Humanidades-II.pdf"target="_blank">Humanidades II</a></li>
    	<li><a href="docs/basica-especifica/6to-semestre-especifica/C-tecnologia-II.pdf"target="_blank">Ciencia Y Tecnología II</a></li>
    </ul>
    </div> -->
          </div>
          <!--tabla1-->

          <div class="col-md-6 tablaLab">
            <div class="flaboral"><h3>Formación laboral</h3></div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">4<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/AFyQ_LQ.pdf"
                    target="_blank"
                    >Análisis Físicos y Químicos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/Correccion-edicion-fot.pdf"
                    target="_blank"
                    >Corrección y Edición Fotográfica</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/Crear-adm-bsdt.pdf"
                    target="_blank"
                    >Crear y Administrar Bases de Datos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/Dibujo-PAE.pdf"
                    target="_blank"
                    >Dibujo de Planos Arquitectónicos y Estructurales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/Elab-Edos-Fin.pdf"
                    target="_blank"
                    >Elaboración de Estados Financieros</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/GestionPersonal.pdf"
                    target="_blank"
                    >Gestión de Personal</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/Preparacion_alimentos.pdf"
                    target="_blank"
                    >Preparación de alimentos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/4to-semestre/Serv-usuarios.pdf"
                    target="_blank"
                    >Servicios a Usuarios</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">6<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/Auditoria_nocturna.pdf"
                    target="_blank"
                    >Auditoria nocturna</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/con-docs.pdf"
                    target="_blank"
                    >Conservación de Documentos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/dis-2D-web.pdf"
                    target="_blank"
                    >Diseño en 2D para Web</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/GCL_LQ.pdf"
                    target="_blank"
                    >Gestión de Calidad de un Laboratorio</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/proy-int.pdf"
                    target="_blank"
                    >Proyecto Integrador</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/int-trabajo.pdf"
                    target="_blank"
                    >Introducción al Trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/PrevRiesTrab.pdf"
                    target="_blank"
                    >Prevención de Riesgos de Trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/prog-pg-web.pdf"
                    target="_blank"
                    >Programación de páginas Web</a
                  >
                </li>
                <li>
                  <a
                    href="docs/20-30 abril-FLAESC/6to-semestre/int-proy.pdf"
                    target="_blank"
                    >Integración de proyectos</a
                  >
                </li>
              </ul>
            </div>
          </div>
          <!--tabla2-->
        </div>
      </div>
    </div>
    <!-- fin Content -->

<?php include('footer.php');?>
