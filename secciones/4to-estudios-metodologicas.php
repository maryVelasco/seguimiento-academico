<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">Programas de estudio: Orientaciones metodológicas</h1>
      </div>
      <hr />

      <div class="container">
        <div class="row">
          <div class="headsec1 col-md-12">
            <h4 class="py-2">4<sup>o</sup> Semestre</h4>
          </div>
          <div class="col-md-12 bdsec">
            <ul class="listmat">
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/biologia-I.pdf"
                  target="_blank"
                  >Biolog&iacute;a I</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/geografia-II.pdf"
                  target="_blank"
                  >Geograf&iacute;a II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/historia-de-mexico.pdf"
                  target="_blank"
                  >Historia de M&eacute;xico</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/lengua-y-literatura-II.pdf"
                  target="_blank"
                  >Lengua y Literatura II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/matematicas-IV.pdf"
                  target="_blank"
                  >Matem&aacute;ticas IV</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/orientacio-II.pdf"
                  target="_blank"
                  >Orientacion II</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/quimica-III.pdf"
                  target="_blank"
                  >Qu&iacute;mica III</a
                >
              </li>
              <li>
                <img src="pdf.svg" height="30" /><a
                  href="docs/4to-semestre-Or-Met/TIC4.pdf"
                  target="_blank"
                  >Tecnologías de la Información y la Comunicación IV</a
                >
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- fin Content -->

  <?php include('footer.php');?>