<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="docs/img-secciones/tira-guias.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Guías de estudio para formación laboral
        </h1>
        <hr />
        <br />
      </div>
      <!-- 
      <div class="container">
        <div class="row mb-4 ml-5">
          <img src="pdf.svg" height="30" class="mr-2" />
          <button type="button" class="btn btn-light">
            <a href="docs/seguimiento-academico.pdf" target="pdfreader" target="_blank"
              >Seguimiento académico marco general</a
            >
          </button>
        </div>
      </div> -->

      <div class="container">
        <div class="row">
          <div class="col-md-6 tablabasi-esp">
            <div class="flaboral2">
              <h3>3° semestre</h3>
            </div>
            <br />
            <div class="bgcaja">
              <ul>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/AtencionHuesped_20B.pdf"
                    target="pdfreader"
                    title="Atención al Huésped"
                    >Atención al Huésped</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/ComunicacionGrafica_20B.pdf"
                    target="pdfreader"
                    title="Comunicación Gráfica<"
                    >Comunicación Gráfica</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/DibujoTecnico_20B.pdf"
                    target="pdfreader"
                    title="Dibujo técnico arquitectónico"
                    >Dibujo técnico arquitectónico</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/ProcesoAdmin_20B.pdf"
                    target="pdfreader"
                    title="El Proceso Administrativo en los Recursos Humanos"
                    >El Proceso Administrativo en los Recursos Humanos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/ElaboracionManuales_20B.pdf"
                    target="pdfreader"
                    title="Elaboración de Manuales Organizacionales"
                    >Elaboración de Manuales Organizacionales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/ModeladoSistemas_20B.pdf"
                    target="pdfreader"
                    title="Modelado de Sistemas y Principios de Programación"
                    >Modelado de Sistemas y Principios de Programación</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/OperacionesComerciales_20B.pdf"
                    target="pdfreader"
                    title="Contabilidad de Operaciones Comerciales"
                    >Contabilidad de Operaciones Comerciales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/OrganizacionRecInf_20B.pdf"
                    target="pdfreader"
                    title="Organización de Recursos de Información"
                    >Organización de Recursos de Información</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/ReservacionHuespedes_20B.pdf"
                    target="pdfreader"
                    title="Reservación y Recepción de Huéspedes"
                    >Reservación y Recepción de Huéspedes</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/3er-semestre/TomaTratamiento_20B.pdf"
                    target="pdfreader"
                    title="Toma y tratamiento para análisis de muestras"
                    >Toma y tratamiento para análisis de muestras</a
                  >
                </li> 
              </ul> 
            </div>
          </div>  
            <!--  --------------------------  SEMESTRES DEL PRIMER PERODO DEL AÑO     ----------------------  -->          
            <div class="col-md-6 tablabasi-esp">
            <div class="flaboral2">
              <h3>4° semestre</h3>
            </div>
              <br />
            <div class="bgcaja">
              <ul>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/analisis-fisicos-y-quimicos.pdf"
                    target="pdfreader"
                    title="Análisis físicos y químico"
                    >Análisis físicos y químico</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/crear-y-administrar-bases-de-datos.pdf"
                    target="pdfreader"
                    title="Crear y administrar bases de datos"
                    >Crear y administrar bases de datos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/dibujo-plano-AE.pdf"
                    target="pdfreader"
                    title="Dibujo de planos arquitectónicos y estructurales"
                    >Dibujo de planos arquitectónicos y estructurales</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/edicion-y-correccion-fotografica.pdf"
                    target="pdfreader"
                    title="Edición y corrección fotográfica"
                    >Edición y corrección fotográfica</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/elaboracion-de-estados-financieros.pdf"
                    target="pdfreader"
                    title="Elaboración de estados financieros"
                    >Elaboración de estados financieros</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/gestion-personal.pdf"
                    target="pdfreader"
                    title="Gestión de personal"
                    >Gestión de personal</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/preparacion-de-alimentos.pdf"
                    target="pdfreader"
                    title="Preparación de alimentos"
                    >Preparación de alimentos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/4to-semestre/servicios-a-usuarios.pdf"
                    target="pdfreader"
                    title="Servicios a usuarios"
                    >Servicios a usuarios</a
                  >
                </li>
              </ul>
            </div>
            <br/>
          </div>
        </div>
      </div>
      <br />

        <div class="container">
        <div class="row">
          <div class="col-md-6 tablabasi-esp">
            <div class="flaboral2">
              <h3>5° semestre</h3>
            </div>
            <br />
            <div class="bgcaja">
              <ul>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/Analisis_Instrumental_20B.pdf"
                    target="pdfreader"
                    title="Análisis Instrumental"
                    >Análisis Instrumental</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/CajaRestCajaRecep_20B.pdf"
                    target="pdfreader"
                    title="Caja de Restaurante y Caja de Recepción"
                    >Caja de Restaurante y Caja de Recepción</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/ContribPersonasFisicasMorales_20B.pdf"
                    target="pdfreader"
                    title="Contribuciones de Personas Físicas y Morales"
                    >Contribuciones de Personas Físicas y Morales</a
                  >
                </li>                
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/ControlEfectivo_20B.pdf"
                    target="pdfreader"
                    title="Control de Efectivo"
                    >Control de Efectivo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/DibujoPlanosInstalaciones_20B.pdf"
                    target="pdfreader"
                    title="Dibujo de Planos de Instalaciones"
                    >Dibujo de Planos de Instalaciones</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/DisEditorial_20B.pdf"
                    target="pdfreader"
                    title="Diseño Editorial"
                    >Diseño Editorial</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/PagoPersonal_20B.pdf"
                    target="pdfreader"
                    title="Elaboración del pago de personal"
                    >Elaboración del pago de personal</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/ProgramaconJava_20B.pdf"
                    target="pdfreader"
                    title="Programación en Java"
                    >Programación en Java</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/ServicioRestaurante_20B.pdf"
                    target="pdfreader"
                    title="Servicio de Restaurante"
                    >Servicio de Restaurante</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/5to-semestre/SistBusqRecInf_20B.pdf"
                    target="pdfreader"
                    title="Sistematización, búsqueda y recuperación de la información"
                    >Sistematización, búsqueda y recuperación de la información</a
                  >
                </li> 
              </ul>
            </div>
          </div>
            <!--  --------------------------  SEMESTRES DEL PRIMER PERODO DEL AÑO     ----------------------  >
            <div class="col-md-6 tablabasi-esp">
              <div class="fEspecifica2">
                <h3>6° semestre</h3>
              </div>
              <br />
              <div class="bgcaja">
                <ul>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/auditoria-nocturna.pdf"
                    target="pdfreader"
                    title="Auditoria nocturna"
                    >Auditoria nocturna</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/conservacion-de-documentos.pdf"
                    target="pdfreader"
                    title="Conservación de documentos"
                    >Conservación de documentos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/dis-2d-web.pdf"
                    target="pdfreader"
                    title="Conservación de documentos"
                    >Diseño en 2D para la web</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/gestion-de-calidad-en-un-laboratorio.pdf"
                    target="pdfreader"
                    title="Gestión de calidad de un laboratorio"
                    >Gestión de calidad de un laboratorio</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/integracion-de-proyectos.pdf"
                    target="pdfreader"
                    title="Integración de proyectos"
                    >Integración de proyectos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/introduccion-al-trabajo.pdf"
                    target="pdfreader"
                    title="Introducción al trabajo"
                    >Introducción al trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/prevencion-de-riesgos-de-trabajo.pdf"
                    target="pdfreader"
                    title="Prevención de riesgos de trabajo"
                    >Prevención de riesgos de trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/programacion-de-paginas-web.pdf"
                    target="pdfreader"
                    title="Programación de páginas web"
                    >Programación de páginas web</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/proyecto-integrador.pdf"
                    target="pdfreader"
                    title="Proyecto integrador"
                    >Proyecto integrador</a
                  >
                </li>
              </ul>
            </div> 

            <!--  --------------------------  SEMESTRES DEL PRIMER PERODO DEL AÑO     ----------------------
            <div class="bgcaja">
              <ul>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/auditoria-nocturna.pdf"
                    target="pdfreader"
                    title="Auditoria nocturna"
                    >Auditoria nocturna</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/conservacion-de-documentos.pdf"
                    target="pdfreader"
                    title="Conservación de documentos"
                    >Conservación de documentos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/dis-2d-web.pdf"
                    target="pdfreader"
                    title="Conservación de documentos"
                    >Diseño en 2D para la web</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/gestion-de-calidad-en-un-laboratorio.pdf"
                    target="pdfreader"
                    title="Gestión de calidad de un laboratorio"
                    >Gestión de calidad de un laboratorio</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/integracion-de-proyectos.pdf"
                    target="pdfreader"
                    title="Integración de proyectos"
                    >Integración de proyectos</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/introduccion-al-trabajo.pdf"
                    target="pdfreader"
                    title="Introducción al trabajo"
                    >Introducción al trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/prevencion-de-riesgos-de-trabajo.pdf"
                    target="pdfreader"
                    title="Prevención de riesgos de trabajo"
                    >Prevención de riesgos de trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/programacion-de-paginas-web.pdf"
                    target="pdfreader"
                    title="Programación de páginas web"
                    >Programación de páginas web</a
                  >
                </li>
                <li>
                  <a
                    href="docs/guias/laboral/6to-semestre/proyecto-integrador.pdf"
                    target="pdfreader"
                    title="Proyecto integrador"
                    >Proyecto integrador</a
                  >
                </li>
              </ul>
            </div>   -->
            <br />
          </div>
        </div>
      </div>
    </div>
    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>
