<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>

    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-materiales.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Formación y actualización docente
        </h1>
      </div>
      <hr />

      <div class="container fad" style="margin-top: 50px;">
        <div class="row">
          <div class="col-md-4 text-center mb-4">
            <div class="fad_img">
              <img src="docs/apoyo-psic-docentes/oferta.png" height="150" />
            </div>
            <div class="btn-group fad_naranja" role="group">
              <button
                id="btnGroupDrop1"
                type="button"
                class="btn dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style="background-color: #ff9234; color: #fff;"
              >
                Oferta institucional
              </button>
              <label class="fab_btn">Oferta institucional</label>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a
                  class="dropdown-item"
                  href="https://view.genial.ly/5ede8b4cadf6170d8aac5bab"
                  target="_blank"
                  >Oferta de formación<br />
                  y actualización 2020B</a
                >
              </div>

              <!--   codigo morgan
              <div class="dropdown-menu menu_morgan" aria-labelledby="btnGroupDrop1">
                <ul>
                  <li>
                    <a
                      class="dropdown-item"
                      href="https://view.genial.ly/5ede8b4cadf6170d8aac5bab"
                      target="_blank"
                      >Oferta de formación<br />
                      y actualización 2020B</a
                    ></li>
                  <li>
                    <a
                      class="dropdown-item"
                      href="https://view.genial.ly/5ede8b4cadf6170d8aac5bab"
                      target="_blank"
                      >Oferta de formación<br />
                      y actualización 2020B</a
                    >
                    <ul class="derecha">
                      <li>
                        <a class="dropdown-item" href="docs/seguimiento-academico/formacion-actualizacion/CursosAprendemx.pdf" target="pdfreader">
                        AprendeMx prueba 2</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="docs/seguimiento-academico/formacion-actualizacion/CursosAprendemx.pdf" target="pdfreader">AprendeMx prueba 3</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="docs/seguimiento-academico/formacion-actualizacion/CursosAprendemx.pdf" target="pdfreader">AprendeMx prueba 4</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="docs/seguimiento-academico/formacion-actualizacion/CursosAprendemx.pdf" target="pdfreader">AprendeMx prueba 5</a>
                      </li>
                    </ul>
                    </li>
                  <li>
                    <a
                      class="dropdown-item"
                      href="https://view.genial.ly/5ede8b4cadf6170d8aac5bab"
                      target="_blank"
                      >Oferta de formación<br />
                      y actualización 2020B</a
                    ></li>
                  <li>
                    <a
                      class="dropdown-item"
                      href="https://view.genial.ly/5ede8b4cadf6170d8aac5bab"
                      target="_blank"
                      >Oferta de formación<br />
                      y actualización 2020B</a
                    ></li>
                </ul>
              </div>
              -->
            </div>
          </div>

          <div class="col-md-4 text-center">
            <!--<div>
              <img
                src="docs/apoyo-psic-docentes/actualizacion.png"
                height="150"
              />
            </div>
            <div class="btn-group" role="group">
              <button
                id="btnGroupDrop1"
                type="button"
                class="btn dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style="background-color: #ff9234; color: #fff;"
              >
                Formación y Actualización
              </button>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a
                  class="dropdown-item"
                  href="docs/seguimiento-academico/formacion-actualizacion/OfertaSEP_TEC_UPN_UNAM_MICROSOFT.pdf"
                  target="pdfreader"
                  >Ofertas formativas</a
                >
              </div>
            </div>  -->
          </div>

          <div class="col-md-4 text-center">
            <div class="fad_img">
              <img
                src="docs/apoyo-psic-docentes/actualizacion.png"
                height="150"
              />
            </div>
            <div class="btn-group fad_cafe" role="group">
              <button
                id="btnGroupDrop1"
                type="button"
                class="btn dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style="background-color: #7c3c21; color: #fff;"
              >
                Formación externa
              </button>
              <label class="fab_btn">Formación externa</label>
              <div
                class="dropdown-menu menu_morgan"
                aria-labelledby="btnGroupDrop1"
              >
                <ul>
                  <li>
                    <label>Cursos MEJOREDU</label>
                    <ul>
                      <li>
                        <a
                          class="dropdown-item"
                          href="https://www.gob.mx/mejoredu/articulos/ensenanza-y-aprendizaje-en-tiempos-de-contingencia-dirigido-a-docentes-y-tecnico-docentes-de-educacion-media-superior"
                          target="_blank"
                          >Enseñanza y aprendizaje <br />en tiempos de
                          contingencia</a
                        >
                      </li>
                      <li>
                        <a
                          class="dropdown-item"
                          href="https://www.gob.mx/mejoredu/articulos/los-equipos-directivos-en-tiempos-de-contingencia-dirigido-a-directores-educacion-media-superior"
                          target="_blank"
                          >Los equipos directivos <br />en tiempos de
                          contingencia</a
                        >
                      </li>
                      <li>
                        <a
                          class="dropdown-item"
                          href="https://www.gob.mx/mejoredu/articulos/supervision-escolar-en-tiempos-de-contingencia-dirigido-a-personal-con-funciones-de-supervision-asesoria-tecnica-pedagogica-y-analogas-en-eb-y-ms"
                          >Supervisión escolar <br />en tiempos de
                          contingencia</a
                        >
                      </li>
                    </ul>
                  </li>

                  <li>
                    <a
                      class="dropdown-item"
                      href="docs/seguimiento-academico/formacion-actualizacion/CursosAprendemx.pdf"
                      target="pdfreader"
                      >AprendeMx, SEP</a
                    >
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      href="docs/seguimiento-academico/formacion-actualizacion/EstrategiaMatamaticas1.pdf"
                      target="pdfreader"
                      >Estrategia de enseñanza <br />de matemáticas 1, SEMS</a
                    >
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      href="docs/seguimiento-academico/formacion-actualizacion/EstrategiaMatematicas2.png"
                      target="pdfreader"
                      >Estrategia de enseñanza <br />de matemáticas 2, SEMS</a
                    >
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      href="docs/seguimiento-academico/formacion-actualizacion/video-nuevaEscuela.png"
                      target="pdfreader"
                      >La enseñanza de las matemáticas en la <br />Nueva Escuela
                      Mexicana SEMS-COSDAC</a
                    >
                  </li>
                  <li>
                    <a
<<<<<<< HEAD
                      class="dropdown-item"
                      href="docs/seguimiento-academico/formacion-actualizacion/OfertaSEP_TEC_UPN_UNAM_MICROSOFT.pdf"
                      target="pdfreader"
                      >Ofertas formativas,<br />
                      SEMS-TEC-UPN-UNAM</a
                    >
=======
                    class="dropdown-item"
                    href="docs/seguimiento-academico/formacion-actualizacion/OfertaSEP_TEC_UPN_UNAM_MICROSOFT.pdf"
                    target="pdfreader"
                    >Ofertas formativas,<br/> SEMS-TEC-UPN-UNAM</a>
                  <a
                    class="dropdown-item"
                    href="docs/seguimiento-academico/formacion-actualizacion/trabajoRemoto-AulasVirtuales.png"
                    target="pdfreader"
                    >Diplomado de Gestión de trabajo <br/>remoto y aulas virtuales, SEMS</a
                  >  
>>>>>>> 8cdfea8a81071e9b206b7cda98873b31dfc7b142
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
    </div>

    <style type="text/css">
      /* .menu_morgan {
      } */
      .menu_morgan ul {
        list-style-type: none;
        padding: 0;
      }
      /* .menu_morgan ul li {
      } */
      .menu_morgan ul li label {
        display: block;
        width: 100%;
        padding: 0.25rem 1.5rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
        margin: 0;
      }
      /* .menu_morgan ul li a {
      } */
      .menu_morgan ul li a:hover {
        background-color: #e8e9ea;
      }
      .menu_morgan ul li label:hover {
        background-color: #e8e9ea;
      }
      .menu_morgan ul ul {
        display: none;
        border: 1px solid;
        position: absolute;
        background-color: #ccc;
        margin-left: -200px;
        margin-top: -30px;
      }
      .menu_morgan ul ul.derecha {
        margin-left: 200px;
        margin-top: -30px;
      }
      /* .menu_morgan ul ul li {
      } */
      .menu_morgan ul li:hover ul {
        display: block;
      }
    </style>

    <div style="height: 350px;">&nbsp;</div>

    <!-- fin Content -->

  <?php include('footer.php');?>
