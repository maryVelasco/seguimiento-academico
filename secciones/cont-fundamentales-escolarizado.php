<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!--  <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">Contenidos esenciales (escolarizado)</h1>
      </div>
      <hr />
      <div style="height: 50px;"></div>
      <div class="container">
        <div class="row">
          <table class="table table-borderless tabla-escolarizado">
            <tbody>

              <tr> <!-- fila 1 -->
                <td>
                  <div class="mb-2">
                    <img src="docs/img-secciones/pdf-downlodad.png" height="60" />
                  </div>
                  <h5>
                    <a href="docs/semeste2020-B/Contenidosesenciales_2020B.pdf" target="_blank">Contenidos esenciales 2020-B</a>
                  </h5>
                </td>
              <tr>
            </tbody>
          </table>
        </div>
      </div>

  </div>
    <!-- fin Content -->

<div style="height: 100px;"></div>
  

    <!-- Footer -->
<?php include('footer.php');?>

<style type="text/css">
  .tabla-escolarizado td{ width:25%; text-align:center;}
</style>