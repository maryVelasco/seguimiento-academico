<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <img
          src="../images/inicio-img/tira-estudiantes.jpg"
          class="img-fluid"
        />
        <h1 class="my-4">Estudiantes</h1>
      </div>
      <hr />

      <div class="container">
        <!-- info -->
        <div class="cajaSecciones">
          <ul>
            <li class="mb-2">
              <a href="guias-estudio.html">Guías de estudio</a>
            </li>
             <li class="mb-2">
              <a href="evaluacion-diagnostica.html">Evaluación diagnóstica</a>
            </li>
             <li class="mb-2">
              <a href="lobas-lobos-grises-covid.html">Lobas y lobos grises frente al COVID19</a>
            </li>
            <li>
              <a href="recomendaciones-estudio-casa.html"
                >Recomendaciones para el estudio en casa</a
              >
            </li>
            <li>
              <a href="materiales-apoyo-las-asignaturas-escolarizado.html"
                >Materiales de apoyo para las asignaturas (escolarizado)</a
              >
            </li>
            <li>
              <a href="https://repositorio.cbachilleres.edu.mx/" target="_blank"
                >Materiales de apoyo para las asignaturas (SEA)</a
              >
            </li>

            <li>
              <a href="creatividad-tiempo-libre.html"
                >Materiales de apoyo para la creatividad y el tiempo libre</a
              >
            </li>
            <li class="mb-2">
              <a href="apoyo-psicologico-alumnos.html"
                >Materiales de apoyo de apoyo psicológico y emocional</a
              >
            </li>

            <li class="mb-2">
              <a href="carpeta-experiencias.html"
                >¿Cómo integro mi portafolio o carpeta de experiencias?</a
              >
            </li>

            <li class="mb-2">
              <a href="jovenes-casa-SEP.html"> Jóvenes en casa-SEP</a>
            </li>
            <li class="mb-2">
              <a href="docs/ResultadosEncuestaEstudiantes.pdf"
                >Resultados de la encuesta a estudiantes
              </a>
            </li>
            <li class="mb-2">
              <a href="mecanismos-regularizacion.html"
                >Mecanismos de Regularización
              </a>
            </li>
          </ul>
        </div>
      </div>
      <!-- info -->
    </div>
    <!-- fin Content -->

    <!-- Footer -->
    <footer>
      <div class="footergob">
        <div class="list-info">
          <div class="container">
            <div class="row">
              <div class="col-sm-3">
                <img
                  data-v-9e928f9a=""
                  src="https://framework-gb.cdn.gob.mx/landing/img/logoheader.svg"
                  href="/"
                  alt="logo gobierno de méxico"
                  class="logo_footer"
                  style="max-width: 90%;"
                />
              </div>

              <div class="col-sm-3">
                <ul>
                  <li>
                    <h5>Enlaces</h5>
                  </li>
                  <li>
                    <a
                      href="https://participa.gob.mx"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Participa</a
                    >
                  </li>
                  <li>
                    <a
                      href="https://www.gob.mx/publicaciones"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Publicaciones Oficiales</a
                    >
                  </li>
                  <li>
                    <a
                      href="http://www.ordenjuridico.gob.mx"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Marco Jurídico</a
                    >
                  </li>
                  <li>
                    <a
                      href="https://consultapublicamx.inai.org.mx/vut-web/"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Plataforma Nacional de Transparencia</a
                    >
                  </li>
                </ul>
              </div>
              <div class="col-sm-3">
                <h5>¿Qué es gob.mx?</h5>

                <p>
                  Es el portal único de trámites, información y participación
                  ciudadana.
                  <a href="https://www.gob.mx/que-es-gobmx">Leer más</a>
                </p>
              </div>
              <div class="col-sm-3">
                <h5>Contacto</h5>
                <p>Dudas e información a</p>
                <p>mesdeayuda@bachilleres.edu.mx</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        style="
          background-image: url('../gobmx/pleca.png');
          background-color: #13322b;
          background-repeat: repeat-x;
          height: 33px;
        "
      ></div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../scripts-boots/jquery/jquery.min.js"></script>
    <script src="../scripts-boots/bootstrap/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
