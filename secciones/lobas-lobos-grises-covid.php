<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!--  <img src="docs/img-secciones/IMG_5450.JPG" class="img-fluid" /> -->
        <h1 class="my-4">Lobas y lobos grises frente al COVID19</h1>
      </div>
      <hr />
      <br /><br />

      <div class="cont-lobos"><!-- info -->
      <p>
      El Colegio de Bachilleres desde sus inicios ha sido una institución promotora de la formación artística y deportiva a través de talleres paraescolares y, en su historia reciente, asignaturas. <br>
      El impacto mundial que ha tenido la pandemia ocasionada por COVID19 y que implicó cerrar todas las escuelas del país, fue aprovechado por el área paraescolar hacia finales del semestre 2020-A como una oportunidad para promover la reflexión, la creatividad, el espíritu deportivo e incentivar a las y los estudiantes de los talleres a participar en concursos y retos. Esta acción puso de manifiesto que, a pesar de las desafortunadas situaciones, la creatividad, el talento y el ánimo por enfrentar las dificultades es un signo distintivo de la fortaleza con la que cuenta la comunidad del Colegio de Bachilleres.<br>
      A través del presente micrositio compartimos los resultados de dichas actividades y hacemos un reconocimiento a todas las y los estudiantes, así como a sus docentes y responsables paraescolares que participaron con gran ánimo y entusiasmo.</p>
      <div class="cont-icons">
        <div class="icon1"><a href="https://sway.office.com/xcG8bWc6kwiAS5Kw?ref=Link" target="_blank"><img src="docs/img-secciones/icon-artes.png" tile="Artes Plásticas"></a> </div>
       
        <div class="icon3"><a href="https://sway.office.com/oGLFSCAWUjDXTAK2" target="_blank"><img src="docs/img-secciones/icon-futbol.png" tile="Futbol"></a> </div>
        <div class="icon4"><a href="https://sway.office.com/HKl7Tkvjr7NG1UUT" target="_blank"><img src="docs/img-secciones/icon-musica.png" tile="Música"></a> </div>
        <div class="icon5"><a href="https://sway.office.com/XIu4Yippxuvp9j5s" target="_blank"><img src="docs/img-secciones/icon-teatro.png" tile="Teatro"></a>  </div>
      </div>
      
        <div class="cont-icons2">
           <div class="icon2"><img src="docs/img-secciones/icon-danza.png" tile="Danza"><br>
             <div class="boton-disp">
              <div
                class="btn-group "
                role="group"
                aria-label="Button group with nested dropdown"
              >
                <div class="btn-group" role="group">
                  <button
                    id="btnGroupDrop1"
                    type="button"
                    class="btn dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    style="background: #f96d80;"
                  >
                    Actividades de danza
                  </button>
                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="https://sway.office.com/nmY7uYd2Q49b3Wii" target="_blank" 
                      >Tik tok</a
                    >
                    <a class="dropdown-item" href="https://sway.office.com/rypl39HQUOIQLDy0" target="_blank"
                      >Fotografías</a
                    >
                  </div>
                </div>
              </div>
            </div>  
        </div>
        </div>
      
      </div>
    </div>
     
     
      <div style="height: 150px;">&nbsp;</div>
    
    
    <!-- fin Content -->

    <!-- Footer -->
  <?php include('footer.php');?>
