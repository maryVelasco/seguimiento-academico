<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>

    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="docs/img-secciones/tira-guias.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Guías de estudio
        </h1>
        <hr />
        <br />
      </div>
      <!-- 
      <div class="container">
        <div class="row mb-4 ml-5">
          <img src="pdf.svg" height="30" class="mr-2" />
          <button type="button" class="btn btn-light">
            <a href="docs/seguimiento-academico.pdf" target="_blank"
              >Seguimiento académico marco general</a
            >
          </button>
        </div>
      </div> -->

      <div class="container">
        <div class="row">
          <div class="col-md-6 tablabasi-esp">
            <div class="fEspecifica2">
              <h3>Formación básica y específica</h3>
            </div>
            <br />
            <div class="bgcaja">
              <h5 class="text-center" class="text-center">
                2<sup>o</sup> Semestre
              </h5>
              <ul>
                <li> <a href="2do-semestre/actividades-fisicas-y-deportivas-II.pdf"
                    title="Actividades f&iacute;sicas y deportivas II"
                    >Actividades f&iacute;sicas y deportivas II</a></li>
                <li> <a href="2do-semestre/apreciacion-artistica-II.pdf"
                    title="Apreciación artística II">Apreciación artística II</a></li>
                <li> <a href="2do-semestre/ciencias-sociales-II.pdf"
                    title="Ciencias sociales II">Ciencias sociales II</a></li>
                <li> <a href="2do-semestre/etica.pdf" title="Ética">Ética</a></li>
                <li> <a href="2do-semestre/fisica-II.pdf" title="Física II">Física II</a></li>
                <li> <a href="2do-semestre/ingles-II.pdf" title="Inglés II">Inglés II</a></li>
                <li> <a href="2do-semestre/lenguaje-y-comunicacion-II.pdf"
                    title="Lenguaje y comunicación II">Lenguaje y comunicación II</a></li>
                <li> <a href="2do-semestre/matematicas-II.pdf" title="Matemáticas II">Matemáticas II</a></li>
                <li>
                  <a href="2do-semestre/quimica-I.pdf" title="Química I"
                    >Química I</a
                  >
                </li>
                <li>
                  <a
                    href="2do-semestre/tic-II.pdf"
                    title="Tecnologías de la información y la comunicación II"
                    >Tecnologías de la información y la comunicación II</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">4<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a href="4to-semestre/biologia-I.pdf" title="Biología I"
                    >Biología I</a
                  >
                </li>
                <li>
                  <a href="4to-semestre/geografia-II.pdf" title="Geografía II"
                    >Geografía II</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre/historia-de-mexico-II.pdf"
                    title="Historia de México II"
                    >Historia de México II</a
                  >
                </li>
                <li>
                  <a href="4to-semestre/ingles-IV.pdf" title="Ingles IV"
                    >Ingles IV</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre/literatura-II.pdf"
                    title="Lengua y literatura II"
                    >Lengua y literatura II</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre/matematicas-IV.pdf"
                    title="Matemáticas IV"
                    >Matemáticas IV</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre/orientacion-II.pdf"
                    title="Orientación II"
                    >Orientación II</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre/quimica-III.pdf"
                    title="Guía de química III"
                    >Guía de química III</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre/tic-IV.pdf"
                    title="Tecnologías de la información y la comunicación IV"
                    >Tecnologías de la información y la comunicación IV</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">6<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a href="6to-semestre/ciencia-y-tecnologia-II.pdf" title=""
                    >Ciencia y tecnología II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/ecologia.pdf" title="">Ecología</a>
                </li>
                <li>
                  <a href="6to-semestre/esem-II.pdf" title=""
                    >Estructura socioeconómica de México II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/humanidades-II.pdf" title=""
                    >Humanidades II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/ingenieria-fisica-II.pdf" title=""
                    >Ingeniería Física II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/ingles-VI.pdf" title="">Inglés VI</a>
                </li>
                <li>
                  <a
                    href="6to-semestre/interdisciplina-artistica-II.pdf"
                    title=""
                    >Interdisciplina artística II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/matematicas-VI.pdf" title=""
                    >Matemáticas VI</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/problemas-filosoficos.pdf" title=""
                    >Problemas filosóficos</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/procesos-industriales.pdf" title=""
                    >Procesos industriales</a
                  >
                </li>
                <li>
                  <a
                    href="6to-semestre/proyectos-de-gestion-social-II.pdf"
                    title=""
                    >Proyectos de gestión social II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/proyectos-de-inversion-II.pdf" title=""
                    >Proyectos de inversión y finanzas personales II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/salud-humana.pdf" title=""
                    >Salud humana II</a
                  >
                </li>
                <li>
                  <a href="6to-semestre/tapt-II.pdf" title=""
                    >Taller de análisis y producción de textos II</a
                  >
                </li>
              </ul>
            </div>
            <br />
          </div>
          <!--tabla1-->

          <div class="col-md-6 tablaLab">
            <div class="flaboral2"><h3>Formación laboral</h3></div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">4<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a
                    href="4to-semestre\analisis-fisicos-y-quimicos.pdf"
                    title="Análisis físicos y químico"
                    >Análisis físicos y químico</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre\crear-y-administrar-bases-de-datos.pdf"
                    title="Crear y administrar bases de datos"
                    >Crear y administrar bases de datos</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre\dibujo-plano-AE.pdf"
                    title="Dibujo de planos arquitectónicos y estructurales"
                    >Dibujo de planos arquitectónicos y estructurales</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre\edicion-y-correccion-fotografica.pdf"
                    title="Edición y corrección fotográfica"
                    >Edición y corrección fotográfica</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre\elaboracion-de-estados-financieros.pdf"
                    title="Elaboración de estados financieros"
                    >Elaboración de estados financieros</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre\gestion-personal.pdf"
                    title="Gestión de personal"
                    >Gestión de personal</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre\preparacion-de-alimentos.pdf"
                    title="Preparación de alimentos"
                    >Preparación de alimentos</a
                  >
                </li>
                <li>
                  <a
                    href="4to-semestre\servicios-a-usuarios.pdf"
                    title="Servicios a usuarios"
                    >Servicios a usuarios</a
                  >
                </li>
              </ul>
            </div>
            <br />

            <div class="bgcaja">
              <h5 class="text-center">6<sup>o</sup> Semestre</h5>
              <ul>
                <li>
                  <a href="6to-semestre\auditoria-nocturna.pdf" title=""
                    >Auditoria nocturna</a
                  >
                </li>
                <li>
                  <a href="6to-semestre\conservacion-de-documentos.pdf" title=""
                    >Conservación de documentos</a
                  >
                </li>
                <li>
                  <a
                    href="6to-semestre\gestion-de-calidad-en-un-laboratorio.pdf"
                    title=""
                    >Gestión de calidad de un laboratorio</a
                  >
                </li>
                <li>
                  <a href="6to-semestre\integracion-de-proyectos.pdf" title=""
                    >Integración de proyectos</a
                  >
                </li>
                <li>
                  <a href="6to-semestre\introduccion-al-trabajo.pdf" title=""
                    >Introducción al trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="6to-semestre\prevencion-de-riesgos-de-trabajo.pdf"
                    title=""
                    >Prevención de riesgos de trabajo</a
                  >
                </li>
                <li>
                  <a
                    href="6to-semestre\programacion-de-paginas-web.pdf"
                    title=""
                    >Programación de páginas web</a
                  >
                </li>
                <li>
                  <a href="6to-semestre\proyecto-integrador.pdf" title=""
                    >Proyecto integrador</a
                  >
                </li>
              </ul>
            </div>
          </div>
          <!--tabla2-->
        </div>
      </div>
    </div>
    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>
