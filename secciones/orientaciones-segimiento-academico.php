<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Orientaciones para el seguimiento académico y recursos didácticos
        </h1>
      </div>
      <hr />

      <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-md-6 text-center">
                  <div>
                    <img src="docs/apoyo-psic-docentes/maestro.png" height="150" />
                  </div>
                  <div class="btn-group" role="group">
                    <button
                      id="btnGroupDrop1"
                      type="button"
                      class="btn dropdown-toggle"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      style="background-color: #633a82; color: #fff;"
                    >
                      Orientaciones pedagógicas
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                      <a class="dropdown-item" href="docs/seguimiento-academico/pedagogicas/RelatoPedagogico.pdf"
                        target="pdfreader">Cómo hacer un relato pedagógico</a>
                      <a class="dropdown-item" href="docs/seguimiento-academico/pedagogicas/GuiaCursoLinea.pdf"
                        target="pdfreader" >Guía rápida para preparar tu curso en línea</a>
                      <a class="dropdown-item" href="docs/seguimiento-academico/pedagogicas/DisenoCursos.pdf"
                        target="pdfreader">Orientaciones para diseño de cursos en línea</a>
                      <a class="dropdown-item" href="docs/seguimiento-academico/pedagogicas/PortafolioEvidencias.pdf"
                        target="pdfreader">¿Qué es un portafolio de evidencias?</a>
                      <a class="dropdown-item" href="docs/seguimiento-academico/pedagogicas/evidencias-orientaciones-evaluacion.pdf"
                        target="pdfreader">Evaluación a través del Portafolio de evidencias</a>
                    </div>
                  </div>
                </div>

              <div class="col-md-6 text-center">
                    <div>
                      <img src="docs/apoyo-psic-docentes/rec-dic.png" height="150" />
                    </div>
                    <div class="btn-group" role="group">
                      <button
                        id="btnGroupDrop1"
                        type="button"
                        class="btn dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        style="background-color: #e32249; color: #fff;"
                      >
                        Recursos didácticos
                      </button>
                      <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item" href="docs/seguimiento-academico/didacticos/ElaborarCuestionarios.pdf"
                          target="pdfreader">Aplicación para elaborar cuestionarios</a>
                        <a class="dropdown-item" href="docs/seguimiento-academico/didacticos/ApoyosDisciplinares.pdf"
                          target="pdfreader">Apoyos disciplinares</a>
                        <a class="dropdown-item" href="docs/seguimiento-academico/didacticos/LibrosAccesoLibre.pdf"
                          target="pdfreader">Catálogos de libros de acceso libre</a>
                        <a class="dropdown-item" href="docs/seguimiento-academico/didacticos/Organizadores.pdf"
                          target="pdfreader">Organizadores gráficos</a>
                      </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <br />


    <div style="height: 350px;">&nbsp;</div>

    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>