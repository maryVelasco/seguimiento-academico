<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->

    <div class="container mb-4">
      <!-- <img src="../images/inicio-img/tira-directores.jpg" class="img-fluid" /> -->
      <div class="container text-center">
        <h1 class="my-4">Recomendaciones para el estudio en casa</h1>
        <hr />

        <br /><br /><br />
      </div>

      <div class="container">
        <div class="col-md-12 infos">
          <ul>
            <li>
              <div
                class="popup_image_header naranja"
                onclick="popup_image_a('image_2')"
              >
                <img
                  src="docs/recomendaciones-estudio-casa/habitos-estudio.jpg"
                  alt=""
                />
              </div>
              <div class="popup_image" id="image_2">
                <div class="img naranja">
                  <img
                    src="docs/recomendaciones-estudio-casa/habitos-estudio.jpg"
                    alt=""
                  />
                </div>
              </div>
            </li>
            <li>
              <div
                class="popup_image_header azul"
                onclick="popup_image_a('image_4')"
              >
                <img
                  src="docs/recomendaciones-estudio-casa/tecnica-de-estudio.jpg"
                  alt=""
                />
              </div>
              <div class="popup_image" id="image_4">
                <div class="img azul">
                  <img
                    src="docs/recomendaciones-estudio-casa/tecnica-de-estudio.jpg"
                    alt=""
                  />
                </div>
              </div>
            </li>
            <li>
              <div
                class="popup_image_header verde"
                onclick="popup_image_a('image_3')"
              >
                <img
                  src="docs/recomendaciones-estudio-casa/plan-de-trabajo.jpg"
                  alt=""
                />
              </div>
              <div class="popup_image" id="image_3">
                <div class="img verde">
                  <img
                    src="docs/recomendaciones-estudio-casa/plan-de-trabajo.jpg"
                    alt=""
                  />
                </div>
              </div>
            </li>

             

            

          </ul>
        </div>
        <div style="height: 250px;"></div>
      </div>
    </div>
    <!-- fin Content -->

    <!-- Footer -->
    <footer>
      <div class="footergob">
        <div class="list-info">
          <div class="container">
            <div class="row">
              <div class="col-sm-3">
                <img
                  data-v-9e928f9a=""
                  src="https://framework-gb.cdn.gob.mx/landing/img/logoheader.svg"
                  href="/"
                  alt="logo gobierno de méxico"
                  class="logo_footer"
                  style="max-width: 90%;"
                />
              </div>
              <div class="col-sm-3">
                <ul>
                  <li>
                    <h5>Enlaces</h5>
                  </li>
                  <li>
                    <a
                      href="https://participa.gob.mx"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Participa</a
                    >
                  </li>
                  <li>
                    <a
                      href="https://www.gob.mx/publicaciones"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Publicaciones Oficiales</a
                    >
                  </li>
                  <li>
                    <a
                      href="http://www.ordenjuridico.gob.mx"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Marco Jurídico</a
                    >
                  </li>
                  <li>
                    <a
                      href="https://consultapublicamx.inai.org.mx/vut-web/"
                      target="_blank"
                      rel="noopener"
                      title="Enlace abre en ventana nueva"
                      >Plataforma Nacional de Transparencia</a
                    >
                  </li>
                </ul>
              </div>
              <div class="col-sm-3">
                <h5>¿Qué es gob.mx?</h5>

                <p>
                  Es el portal único de trámites, información y participación
                  ciudadana.
                  <a href="https://www.gob.mx/que-es-gobmx">Leer más</a>
                </p>
              </div>
              <div class="col-sm-3">
                <h5>Contacto</h5>
                <p>Dudas e información a</p>
                <p>mesdeayuda@bachilleres.edu.mx</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        style="
          background-image: url('../gobmx/pleca.png');
          background-color: #13322b;
          background-repeat: repeat-x;
          height: 33px;
        "
      ></div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../scripts-boots/jquery/jquery.min.js"></script>
    <script src="../scripts-boots/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
      function popup_image_a(e) {
        var id = "#" + e;
        $(id).show();

        console.log("mostrando " + id);

        return null;
      }

      $(".popup_image").click(function () {
        $(".popup_image").hide();
        console.log("ocultando infos");
      });
    </script>
  </body>
</html>
