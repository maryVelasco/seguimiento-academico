<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img src="../images/inicio-img/tira-docentes.jpg" class="img-fluid" /> -->
        <h1 class="my-4">Orientaciones metodológicas y disciplinares</h1>
      </div>
      <hr />

      <div class="container">
        <!-- info -->
        <div class="row mb-4 ml-5">
          <img src="pdf.svg" height="30" class="mr-2" />
          <button type="button" class="btn btn-light">
            <a
              href="docs/programas-estudio/orientaciones-metodolog-intro.pdf"
              target="_blank"
              >Seguimiento académico marco general</a
            >
          </button>
          <br />
        </div>
      </div>
      <!-- info -->
      <!--<div class="container">
        <div class="row">
          <div class="col-md-12 bdsec2">
            <ul class="listmat2">
              <li class="mb-3">
                <a href="doc-estudios-metodologicas.php">
                  2° semestre Orientacion Metodológica</a
                >
              </li>
              <li class="mb-3">
                <a href="4to-estudios-metodologicas.php">
                  4° semestre Orientacion Metodológica</a
                >
              </li>
              <li class="mb-3">
                <a href="6to-estudios-metodologicas.php">
                  6° semestre Orientacion Metodológica</a
                >
              </li>
            </ul>
          </div>
        </div>
      </div>-->
      <br /><br /><br />
    </div>
    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>
