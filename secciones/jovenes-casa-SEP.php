<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
        <!-- <img  src="../images/inicio-img/tira-estudiantes.jpg" class="img-fluid"/> -->
        <h1 class="my-4">Jóvenes en casa-SEP</h1>
      </div>
      <hr />

      <div class="container" style="margin-top: 50px;">
        <div class="row">
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td>
                  <div class="circulo" style="background-color: #e6e6e6;">
                    <img src="docs/icons/tv.svg" height="60" />
                  </div>
                </td>
                <td class="align-middle">
                  <h5>
                    <a
                      title="ver archivo"
                      href="http://jovenesencasa.educacionmediasuperior.sep.gob.mx/jovenes-en-tv/"
                      target="_blank"
                      >Horarios de transmisión</a
                    >
                  </h5>
                </td>

                <td>
                  <div class="circulo" style="background-color: #cecef6;">
                    <img src="docs/icons/logo_jovenes_1x-1.png" height="60" />
                  </div>
                </td>
                <td class="align-middle">
                  <h5>
                    <a
                      title="ver archivo"
                      href="http://jovenesencasa.educacionmediasuperior.sep.gob.mx/"
                      target="_blank"
                      >Información de SEP
                    </a>
                  </h5>
                </td>

                <td>
                  <div class="circulo" style="background-color: #e6e6e6;">
                    <img src="docs/icons/youtube.svg" height="60" />
                  </div>
                </td>
                <td class="align-middle">
                  <h5>
                    <a
                      title="ver archivo"
                      href="https://www.youtube.com/aprendeencasa"
                      target="_blank"
                      >Canal de Youtube</a
                    >
                  </h5>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <!--submenus -->
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-4 text-center">
            <!-- <div
              class="circulo"
              style="background-color: #e6e6e6; margin-bottom: 10px;"
            >
              <img src="docs/icons/agendas.svg" height="80" />
            </div> -->
            <!-- <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #a9d0f5;"
                >
                  Cuadernillos para estudiantes
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-2.pdf"
                    target="pdfreader"
                    >Fascículo 2</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-3.pdf"
                    target="pdfreader"
                    >Fascículo 3</a
                  >
                  <a class="dropdown-item" href="" target="pdfreader"
                    >Fascículo 4</a
                  >
                </div>
              </div>
            </div> -->
          </div>
          <div class="col-md-4 text-center">
            <div
              class="circulo"
              style="background-color: #a9f5e1; margin-bottom: 10px;"
            >
              <img src="docs/icons/agendas.svg" height="80" />
            </div>
            <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #a9d0f5;"
                >
                  Cuadernillos para estudiantes
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-2.pdf"
                    target="pdfreader"
                    >Fascículo 2</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-3.pdf"
                    target="pdfreader"
                    >Fascículo 3</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-4.pdf"
                    target="pdfreader"
                    >Fascículo 4</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-6.pdf"
                    target="pdfreader"
                    >Fascículo 6</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-7.pdf"
                    target="pdfreader"
                    >Fascículo 7</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-8.pdf"
                    target="pdfreader"
                    >Fascículo 8</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/Cuadernillo_fasciculo-9.pdf"
                    target="pdfreader"
                    >Fascículo 9</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/Cuadernillo_fasciculo-10.pdf"
                    target="pdfreader"
                    >Fascículo 10</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/Cuadernillo_fasciculo-11.pdf"
                    target="pdfreader"
                    >Fascículo 11</a
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <!-- <div
              class="circulo"
              style="background-color: #e6e6e6; margin-bottom: 10px;"
            >
              <img src="docs/icons/agendas.svg" height="80" />
            </div> -->
            <!-- <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #a9d0f5;"
                >
                  Cuadernillos para estudiantes
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-2.pdf"
                    target="pdfreader"
                    >Fascículo 2</a
                  >
                  <a
                    class="dropdown-item"
                    href="docs/fasciculos/fasciculo-3.pdf"
                    target="pdfreader"
                    >Fascículo 3</a
                  >
                  <a class="dropdown-item" href="" target="pdfreader"
                    >Fascículo 4</a
                  >
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>

      <div style="height: 150px;">&nbsp;</div>

      <br /><br /><br /><br /><br /><br />
    </div>
    <!-- fin Content -->

    <!-- Footer -->
<?php include('footer.php');?>
