<?php include('header.php'); ?>

<?php
/* Carrusel Morgan v1.0 */

// aqui se configura la carpeta de donde se tomaran las imagenes
$img_dir = '../images/galerias/';

$imgs = array(
	array('file' => '1.jpg', "title" => '', "index" => 1 ),
	array('file' => '2.jpg', "title" => '', "index" => 2 ),
	array('file' => '4.JPG', "title" => '', "index" => 3 ),
	array('file' => '5.jpg', "title" => '', "index" => 4 ),
	array('file' => '6.png', "title" => '', "index" => 5 ),
	array('file' => '7.jpg', "title" => '', "index" => 6 ),
	array('file' => '9.jpg', "title" => '', "index" => 7 ),
	array('file' => '10.jpg', "title" => '', "index" => 8 ),
	array('file' => '11.JPG', "title" => '', "index" => 9 ),
	array('file' => '13.JPG', "title" => '', "index" => 10 ),
	array('file' => '14.JPG', "title" => '', "index" => 11 ),
	array('file' => '15.jpg', "title" => '', "index" => 12 ),
	array('file' => '16.jpg', "title" => '', "index" => 13 ),
	array('file' => '17.jpg', "title" => '', "index" => 14 ),
	array('file' => '19.jpg', "title" => '', "index" => 15 ),
);

?>

<!-- carrusel -->
<div class="crrslM" style="width: 80%;">
	<div class="crrslM_cont">
		<div class="crrslM_elem"><img src="" id="crrslM_img"></div>
		<div class="crrslM_button cm_preview"><div class="ico"></div></div>
		<div class="crrslM_button cm_next"><div class="ico"></div></div>
	</div>

	<!-- list thumbs -->
	<div class="crrslM_thumbs">
		<div class="crrslM_thumbs_left"><div class="ico"></div></div><?php
		?><div class="crrslM_thumbs_cont">
			<ol id="crrslMt_lits" class=""><?php

				$n=1;
				foreach ($imgs as $et => $r) {

					echo "\n";

					$active = '';
					if($n==1){ $active = ' class="active"'; }

					?><li<?php echo $active; ?>><?php
						?><div class="crrslM_thumb" data="<?php echo $img_dir.$r['file']; ?>" id="img-<?php echo $n; ?>"><?php
							?><img src="<?php echo $img_dir.$r['file']; ?>" alt="" data-index="<?php echo $n; ?>"><?php
							?><span><?php echo $n; ?></span><?php
						?></div><?php
					?></li><?php
					$n++;
				}

			?></ol>
		</div>
		<div class="crrslM_thumbs_rigth"><div class="ico"></div></div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="../plugins/carruselM/css/morgan_carrusel.css?v=159267">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="../plugins/carruselM/js/morgan_carrusel.js"></script>

<script type="text/javascript">

var carruselM = null;

window.onload = function() {
	carruselM = carruselM_init();
}

</script>
<!-- fin carrusel -->

<?php include('footer.php'); ?>
