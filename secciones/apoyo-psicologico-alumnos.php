<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>

    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!-- <img src="../images/inicio-img/tira-estudiantes.jpg" class="img-fluid" /> -->
        <h1 class="my-4" style="width: 90%;">
         Material de apoyo psicológico y emocional 
        </h1>
        <hr />
        <br />
      </div>
      <!--tabla contenidos--->
      <!--submenus -->
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-4 text-center">
            <div class="circulo" style="background-color: #e6e6e6; margin-bottom: 10px;">
              <img src="docs/apoyo-psic-alumnos/amor-cvd.png" height="100" />
            </div>
            <div class="btn-group mt-2" role="group" aria-label="Button group with nested dropdown">
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #d0a9f5;">
                  El amor en tiempos <br />del coronavirus
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/12-me_alertasDeViolencia-ATC.pdf" target="pdfreader">Alertas de violencia <br />en el noviazgo</a>
                    <a class="dropdown-item"  href="docs/apoyo-psic-alumnos/21-me_violenciaNoviazgo-ATC.pdf"  target="pdfreader">Violencia en el noviazgo</a>
                    <a class="dropdown-item"  href="docs/apoyo-psic-alumnos/23-me_violenciaSexual-ATC.pdf"    target="pdfreader">Violencia sexual<br />en el noviazgo</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="circulo" style="background-color: #f5d0a9; margin-bottom: 10px;">
              <img src="docs/apoyo-psic-alumnos/familia-amigos.png" height="100"/>
            </div>
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
              <div class="btn-group mt-2" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #04b4ae;">
                  Familia y amigos
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/2-Escucha.pdf" target="pdfreader">¿Escuchas a los demás?</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/6-Empatia.pdf" target="pdfreader">Escuchar con empatía</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/3-MeComunico.pdf" target="pdfreader">¿Cómo me comunico?</a>
                    <a class="dropdown-item" href="https://www.gaceta.unam.mx/wp-content/uploads/2020/05/violencia.pdf"
                      target="pdfreader">Violencia de género y COVID-19</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="circulo" style="background-color: #cef6f5; margin-bottom: 10px;">
              <img src="docs/apoyo-psic-alumnos/paz-interior.png" height="100"/>
            </div>
            <div class="btn-group mt-2" role="group" aria-label="Button group with nested dropdown">
              <div class="btn-group" role="group">
                <button
                      id="btnGroupDrop1"
                      type="button"
                      class="btn dropdown-toggle"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      style="background: #f7be81;">
                      Paz interior
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/14-me_Ansiedad-PI.pdf" target="pdfreader">Ansiedad</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/17-me_Miedo-PI.pdf" target="pdfreader">El miedo</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/5-EstrategiasManejoEstres.pdf" target="pdfreader">Estrategias para el manejo del estrés</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/16-me_Frustracion-PI.pdf" target="pdfreader">Frustración</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/18-me_PrimerosAuxPsic-PI.pdf"
                      target="pdfreader">Primeros Auxilios Psicológicos</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/13-me_Angustia-PI.pdf"target="pdfreader">¿Qué es la angustia?</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/8-resiliencia.pdf" target="pdfreader">¿Qué es la resiliencia?</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/4-Respiracion.pdf" target="pdfreader">Respiración profunda</a>
                    <a class="dropdown-item" href="docs/apoyo-psic-alumnos/24-me_tipsSaludMental-PI.pdf" target="pdfreader">Tips de salud mental</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4 text-center">
            <div class="circulo" style="background-color: #f2f5a9; margin-bottom: 10px;">
              <img src="docs/apoyo-psic-alumnos/alimentacion.png"  height="100"/>
            </div>
            <div class="btn-group mt-2" role="group" aria-label="Button group with nested dropdown">
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #f6cef5;">
                  Eeeres saludable,<br />eeeres bachilleres.
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a class="dropdown-item" href="docs/apoyo-psic-alumnos/11-ejercicio.pdf" target="pdfreader">Cómo nos podemos ejercitar</a>
                  <a class="dropdown-item" href="docs/apoyo-psic-alumnos/19-me_PrevDrogas-ES.pdf" target="pdfreader">Prevención del consumo de drogas</a>
                  <a class="dropdown-item" href="docs/apoyo-psic-alumnos/20-me_RecomendacionesCuidarSalud-ES.pdf" target="pdfreader">Recomendacion es para<br />cuidar la salud</a>
                  <a class="dropdown-item" href="docs/apoyo-psic-alumnos/22-me_VidaSaludable-ES.jpg" target="_blank">Vida saludable</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <!-- <div
              class="circulo"
              style="background-color: #81bef7; margin-bottom: 10px;"
            >
              <img src="docs/apoyo-psic-alumnos/paz-mental.png" height="100" />
            </div> -->

            <!-- <div
              class="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #5882fa;"
                >
                  Paz mental
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a
                    class="dropdown-item"
                    href="https://www.facebook.com/watch/live/?
v=249769976164225&ref=search"
                    target="_blank"
                    >Como ganarle a la ansiedad <br />sin salir de casa</a
                  >
                  <a class="dropdown-item" href="docs/" target="pdfreader"></a>
                  <a class="dropdown-item" href="docs/" target="pdfreader"></a>
                  <a class="dropdown-item" href="docs/" target="pdfreader"></a>
                </div>
              </div>
            </div> -->
          </div>

          <!--<div class="col-md-4 text-center">
            <div class="circulo" style="background-color: #f2f5a9; margin-bottom: 10px; padding-top: 20px;">
              <img src="docs/apoyo-psic-alumnos/auto-ayuda.png" height="70" />
            </div>
            <div class="btn-group mt-2" role="group" aria-label="Button group with nested dropdown">
              <div class="btn-group" role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  class="btn dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style="background: #f5a9a9;">
                  Orientación vocacional
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  
                   <a class="dropdown-item" href="docs/" target="pdfreader"></a>
                  <a class="dropdown-item" href="docs/" target="pdfreader"></a> 
                </div>
              </div>
            </div>
          </div>-->
          
        </div>
      </div>

      <div style="height: 250px;">&nbsp;</div>
      <!-- ------------------------------------------------------------------------- -->

      <!-- fin container tabla -->
      <!-- fin Content -->
    </div>

    <!-- Footer -->
<?php include('footer.php');?>
