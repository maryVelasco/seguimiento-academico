<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
       <!--  <img src="docs/img-secciones/tira-guias.jpg" class="img-fluid" /> -->
        <h1 class="my-4">
          Guías de estudio 2020B
        </h1>
        <hr />

        <br />
      </div>

       <div class="container">
        <div class="col-md-12 bdsec2">
          <ul class="listmat8">
            <li class="mb-3">
              <a href="guias-estudio-formacion-especifica.php"
                >Formación básica y específica</a>
            </li>
            <li>
              <a href="guias-estudio-laboral.php">Formación Laboral</a>
            </li>
          </ul>
        </div>
      </div> 

    </div>

    <div style="height: 250px;"></div>
    <!-- fin Content -->

  <?php include('footer.php');?>
