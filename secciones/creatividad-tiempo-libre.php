<?php 
  $pdir = '../'; 
  include($pdir.'header.php');
?>
    <!----------------------------------------------------------------------------------------->

    <!-- Page Content -->
    <div class="container mb-4">
      <div class="container text-center">
      <!--  <img src="docs/img-secciones/tira-alumnos-2.jpg" class="img-fluid" /> -->
        <h1 class="my-4" style="width: 90%;">
          Artes, ciencia, deportes y más… 
        </h1>
       
        <br />
      </div>
      <!--tabla contenidos--->
      <div class="container">
        <table class="table">
          <tbody>
            <tr>
              
             
            </tr>
            <!----------fila -->
            <tr>
              <td>
                <div class="circulo" style="background-color: #f7be81;">
                  <img src="docs/tiempo-libre/icons/lona.svg" height="60" />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/cinco-pintoras.pdf"
                    target="_blank"
                    >5 pintoras que debes conocer
                  </a>
                </h5>
              </td>
               <td>
                <div class="circulo" style="background-color: #df7401;">
                  <img
                    src="docs/tiempo-libre/icons/musica-his.sg.svg"
                    height="60"
                  />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/musica-historia.pdf"
                    target="_blank"
                    >M&uacute;sica e Historia</a
                  >
                </h5>
              </td>  
            </tr>
            <!----------fila -->

            <!----------fila -->
            <tr>
              <td>
                <div class="circulo" style="background-color: #ffbf00;">
                  <img
                    src="docs/tiempo-libre/icons/palomitas-de-maiz.svg"
                    height="60"
                  />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/el-cine-se-lee.pdf"
                    target="_blank"
                    >El cine tambi&eacute;n se lee</a
                  >
                </h5>
              </td>
                   <td>
                <div class="circulo" style="background-color: #9afe2e;">
                  <img
                    src="docs/tiempo-libre/icons/lista-de-reproduccion.svg"
                    height="60"
                  />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/musica-expande.pdf"
                    target="_blank"
                    >St. Vincent, m&uacute;sica que expande</a
                  >
                </h5>
              </td>
            </tr>
            <!----------fila -->
            <tr>
              <td>
                <div class="circulo" style="background-color: #00bfff;">
                  <img
                    src="docs/tiempo-libre/icons/futbol-americano.svg"
                    height="60"
                  />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/mejor-futbolista.pdf"
                    target="_blank"
                    >El mejor futbolista del mundo</a
                  >
                </h5>
              </td>
              <td>
                <div class="circulo" style="background-color: cornflowerblue;">
                  <img src="docs/tiempo-libre/icons/idea.svg" height="60" />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/gran-idea.pdf"
                    target="_blank"
                    >&iexcl;Qu&eacute; gran idea!</a
                  >
                </h5>
              </td>
            </tr>
            <!----------fila -->
            <tr>
              <td>
                <div class="circulo" style="background-color: #fa5858;">
                  <img
                    src="docs/tiempo-libre/icons/liderazgo.svg"
                    height="60"
                  />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/escolta-bandera.pdf"
                    target="_blank"
                    >Escolta de Bandera</a
                  >
                </h5>
              </td>
              <td>
                <div class="circulo" style="background-color: #642efe;">
                  <img src="docs/tiempo-libre/icons/comenzar.svg" height="60" />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/al-infinito.pdf"
                    target="_blank"
                    >Al infinito y m&aacute;s all&aacute;</a
                  >
                </h5>
              </td>
              
            </tr>
            <!----------fila -->
            <tr>
              <td>
                <div class="circulo" style="background-color: #ac58fa;">
                  <img src="docs/tiempo-libre/icons/piano.svg" height="60" />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/mujeres-musica.pdf"
                    target="_blank"
                    >Mujeres en la m&uacute;sica</a
                  >
                </h5>
              </td>
              <td>
                <div class="circulo" style="background-color: #58fad0;">
                  <img src="docs/tiempo-libre/icons/teclado.svg" height="60" />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/cuarteto-lenguaje.pdf"
                    target="_blank"
                    >El Cuarteto de Nos y el lenguaje como un juego</a
                  >
                </h5>
              </td>
              
            </tr>
            <!----------fila -->
            <tr>
              <td>
                <div class="circulo" style="background-color: #886a08;">
                  <img
                    src="docs/tiempo-libre/icons/exposicion.svg"
                    height="60"
                  />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/paseo-museo.pdf"
                    target="_blank"
                    >Paseo por el museo</a
                  >
                </h5>
              </td>
              <td>
                <div class="circulo" style="background-color: #fa58ac;">
                  <img
                    src="docs/tiempo-libre/icons/bailarina.svg"
                    height="60"
                  />
                </div>
              </td>
              <td class="align-middle">
                <h5>
                  <a
                    title="ver archivo"
                    href="docs/tiempo-libre/la-danza.pdf"
                    target="_blank"
                    >La danza como expresi&oacute;n corporal</a
                  >
                </h5>
              </td>

            </tr>
            <!----------fila -->

          </tbody>
        </table>
      </div>
      <!-- fin container tabla -->
      <!-- fin Content -->
    </div>

    <!-- Footer -->
    <?php include('footer.php');?>
