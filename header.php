<?php

if(!isset($pdir)){
    $pdir = '';
}

?><!DOCTYPE html><?php

include('pmenu.php');

?><html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Huella digital Bachilleres -Espacio de recursos digitales para el acompañamiento académico y socioemocional dirigido a la comunidad del Colegio de Bachilleres-</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo $pdir; ?>scripts-boots/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo $pdir; ?>css/estilos.css?v=159268" rel="stylesheet">

  <!-- font Awesome CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700&amp;subset=latin" media="all">

  <link rel="shortcut icon" href="<?php echo $pdir; ?>images/header/favicon.ico">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163121825-2"></script>
  <script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', 'UA-163121825-2');
  </script>

</head>

<body>
  <div class="debug"></div>
  <header>
    <!-- primera barra -->
    <nav class="navbar navbar-inverse navbar-fixed-top navbar-gob" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarGobCollapse"
            title="Interruptor de Navegación"></button>
          <a class="navbar-brand" href="https://www.gob.mx/">
            <img src="https://framework-gb.cdn.gob.mx/landing/img/logoheader.svg" width="128" height="48"
              alt="Página de inicio, Gobierno de México">
          </a>
        </div>
        <div class="collapse navbar-collapse show" id="navbarGobCollapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="https://www.gob.mx/tramites">Trámites</a></li>
            <li><a href="https://www.gob.mx/gobierno">Gobierno</a></li>
            <li><a href="https://www.gob.mx/busqueda">
                <img src="https://framework-gb.cdn.gob.mx/landing/img/lupa.png"></a></li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- tercera barra -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark fixed-top container-fluid navbar-gaceta">
      <div class="container">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarGaceta"
          title="Interruptor de Navegación"></button>

        <!-- logo principal -->
        <div class="logo logoA"><a href="https://www.gob.mx/bachilleres" target="_blank"><img
              src="<?php echo $pdir; ?>images/header/logoCB.svg"></a></div>

        <!-- menu -->
          <?php if( $seg_academico_menu ){
            ?><div class="collapse navbar-collapse " id="navbarGaceta">
            <ul class="navbar-nav"><?php
              foreach ($seg_academico_menu as $et => $r) {

                // mostrando opciones del menu de primer nivel

                $color = '';
                if( isset( $r['color'] ) && $r['color']!=null ){
                  $color = 'style="background-color: '.$r['color'].';"';
                }

                ?><li class="nav-item" <?php echo $color; ?> ><?php
                  if( isset($r['link']) && $r['link']!=null ){
                    ?><a class="nav-link" <?php 
                        ?>href="<?php echo $r['link']; ?>" <?php 
                        ?>title="<?php echo $r['title']; ?>"
                        <?php echo $color; ?>
                        ><?php echo $r['title']; ?></a><?php 
                  }else{

                    ?><span <?php //echo $color; ?> ><?php echo $r['title'] ?></span><?php
                  } 

                  // mostrando opciones del menu de segundo nivel
                  ?><ul <?php echo $color; ?> ><?php
                    foreach ($r['opc'] as $etr => $rr) {
                      ?><li <?php 
                          if( isset($rr['color']) && $rr['color']!=null ){ 
                              echo 'style="background-color:'.$rr['color'].';"';
                          }
                          if( isset( $rr['resaltado'] ) && $rr['resaltado']!='' ){
                              ?>style="background-color: <?php echo $rr['resaltado']; ?>;"<?php
                          }
                      ?> ><?php
                      if( isset($rr['link']) && $rr['link']!=null ){
                        // inserta link
                        ?><a class="nav-link" <?php 
                          ?>href="<?php echo $rr['link']; ?>" <?php 
                          ?>title="<?php echo $rr['title']; ?>" <?php
                            if( isset($rr['blank']) && $rr['blank']==true ){
                              ?>target="_blank" <?php
                            }
                            if( isset( $rr['resaltado'] ) && $rr['resaltado']!='' ){
                              ?>style="background-color: <?php echo $rr['resaltado']; ?>;"<?php
                            }
                          ?> ><?php 
                              // inserta imagen
                              if( isset($rr['image']) && $rr['image']!=null ){
                                  ?><div class="icon"><img src="<?php echo $pdir.$rr['image']; ?>"></div><?php
                              }
                              // inserta titulo
                              echo $rr['title'];
                          ?></a><?php 
                      }else{
                          if( isset($rr['title']) ){
                              echo '&nbsp;&nbsp;'.$rr['title'];
                          }
                      }
                      ?></li><?php
                    }
                  ?></ul><?php
                ?></li>
             <div class="separadormenu"></div>
             
              <?php
              }
            ?></ul></div><?php
          } ?>
      </div>
    </nav>
  </header>


  