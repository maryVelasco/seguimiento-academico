<?php

function get_host(){
    /*return "//cbgobmx-dev.cbachilleres.edu.mx";
    /*return '//segacademcb.cbachilleres.edu.mx';*/
    return '//'.$_SERVER['HTTP_HOST'];
}

function get_url(){
    /*
    $url = "//cbgobmx-dev.cbachilleres.edu.mx";
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/";
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/index.php";
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/segacad-dev";
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/segacad-dev/";
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/segacad-dev/index.php";
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/segacad-dev/index.xdoc";/*
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/segacad-dev/secciones/index.php";/*
    $url = "//cbgobmx-dev.cbachilleres.edu.mx/segacad-dev/secciones/orientaciones-segimiento-academico.html";
    return $url;*/

    return '//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

function valida_url(){
    if( get_host() == '//localhost' ){
        return get_host().'/seguimiento/';
    }
    if( get_host() == '//huelladigital.cbachilleres.edu.mx' ){
        return get_host().'/';
    }
    
    // busca '/secciones/' si no la encuentra
    $url = get_url();
    $pos = strpos($url, '/secciones/');
    if( $pos === false ){
        // la ulr termina en directorio
        if( $url[ strlen($url)-1 ]=='/' ){
            return $url;    
        }
        // si la url es identica al host regresa $url
        if( $url == get_host() ){
            return $url.'/';
        }
        
        // el ultimo elemento de la url no tiene punto
        $u = explode('/',$url);
        $pos = strpos($u[ count($u)-1 ], '.');
        if( $pos === false ){
            return $url.'/';
        }
        
        // el ultimo elemento de la url si tiene punto
        // excluyendo el ultimo elemento
        
        $u[ count($u)-1 ] = '';
        $url = implode( '/', $u );
        
        return $url;
    }
    
    $url = explode( '/secciones/', $url );
    $url = $url[0].'/';
    
    return $url;
}

$url = valida_url();
//    echo '<pre>'; echo "url final ==> ".( $url ); echo '</pre>';

$seg_academico_menu = array(
    array(
        "title"     => "Inicio",
        "link"      => $url."index.php",
        "image"     => "",
        "color"     => "",
        "resaltado" => "",
        "opc"       => array(),
    ),
    array(
        "title"     => "Estudiantes",
        "link"      => "",
        "image"     => "",
        "color"     => "#7DCec8",
        "resaltado" => "",
        "opc"       => array(
            array(
                "color" => "",
                "link" => $url."secciones/recomendaciones-estudio-casa.php",
                "title" => "Recomendaciones para el estudio en casa",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/guias-estudio.php",
                "title" => "Guías de estudio",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/recursos-apoyo-escolarizado-2020-B.php",
                "title" => "Recursos de apoyo escolarizado 2020-B",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => "https://repositorio.cbachilleres.edu.mx",
                "title" => "Recursos de apoyo SEA 2020-B",
                "image" => "",
                "resaltado" => "",
                "blank" => true,
            ),
            array(
                "color" => "",
                "link" => $url."secciones/creatividad-tiempo-libre.php",
                "title" => "Artes, ciencia, deportes y más…",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/apoyo-psicologico-alumnos.php",
                "title" => "Materiales de apoyo psicológico y emocional",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/lobas-lobos-grises-covid.php",
                "title" => "Lobas y lobos grises frente al COVID19",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/jovenes-casa-SEP.php",
                "title" => "Jóvenes en casa-SEP",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/mecanismos-regularizacion.php",
                "title" => "Mecanismos de Regularización",
                "image" => "",
                "resaltado" => "",
            ),
        ),
    ),
    array(
        "title"     => "Docentes",
        "link"      => "",
        "image"     => "",
        "color"     => "#FD98CA",
        "resaltado" => "",
        "opc"       => array(

            array(
                "color" => "",
                "link" => $url."secciones/metodologicas-disciplinares.php",
                "title" => "Orientaciones metodológicas y disciplinares",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/cont-fundamentales-escolarizado.php",
                "title" => "Contenidos esenciales (escolarizado)",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/orientaciones-segimiento-academico.php",
                "title" => "Orientaciones para el seguimiento académico y recursos didácticos",
                "image" => "",
                "resaltado" => "",
            ),

            array(
                "color" => "",
                "link" => $url."secciones/orientaciones-evaluacion-aprendizaje.php",
                "title" => "Evaluación de los aprendizajes",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/",
                "title" => "Herramientas tecnológicas para la enseñanza",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/formacion-actualizacion-docente.php",
                "title" => "Formación y actualización docente",
                "image" => "",
                "resaltado" => "",
            ),

            array(
                "color" => "",
                "link" => $url."secciones/habilidades-socioemocionales.php",
                "title" => "Habilidades socioemocionales",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."secciones/foro-salud-mental-adolescente.php",
                "title" => "Foros Académicos para Orientadores y Tutores de Acompañamiento",
                "image" => "",
                "resaltado" => "",
            ),
        ),
    ),
    array(
        "title"     => "Equipo directivo",
        "link"      => "",
        "image"     => "",
        "color"     => "#FEBB98",
        "resaltado" => "",
        "opc"       => array(
            array(
                "color" => "",
                "link" => "https://www.gob.mx/bachilleres/articulos/marco-juridico-de-las-medidas-tomadas-por-el-colegio-de-bachilleres-por-el-covid-19",
                "title" => "Disposiciones legales por contingencia",
                "image" => "",
                "resaltado" => "",
                "blank" => true,
            ),
        ),
    ),
    array(
        "title"     => "Madres, padres y tutores",
        "link"      => "",
        "image"     => "",
        //"color"     => "#ffc388",
        "color"     => "#79D6F8",
        "resaltado" => "",
        "opc"       => array(
            array(
                "color" => "",
                "link" => $url."padres-tutores/",
                "title" => "Comunidad educativa del Colegio de Bachilleres ",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."padres-tutores/",
                "title" => "Acompañamiento académico de las y los adolescentes ",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."padres-tutores/",
                "title" => "Vida en familia ",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."padres-tutores/",
                "title" => "Salud en familia  ",
                "image" => "",
                "resaltado" => "",
            ),
            array(
                "color" => "",
                "link" => $url."padres-tutores/",
                "title" => "Conductas de alto riesgo en la adolescencia  ",
                "image" => "",
                "resaltado" => "",
            ),
             array(
                "color" => "",
                "link" => $url."padres-tutores/",
                "title" => "Manejo de duelos ",
                "image" => "",
                "resaltado" => "",
            ),
        ),
    ),
);

?>